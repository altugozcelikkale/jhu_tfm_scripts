function [tfm_fields] = get_fields(TFM3D_directory, app_config)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% load coordinates data with image metadata:
coord_file_path = app_config.get_coord_mat_file_path(TFM3D_directory);
load(coord_file_path, 'coord');
Nt = coord.image_metadata.Nt;

traction_directory_path = app_config.get_traction_directory(TFM3D_directory);
tfm_fields = cell(Nt,1);
for i = 1:Nt
    traction_file_path     = fullfile(traction_directory_path, app_config.generate_numbered_file_name(app_config.traction_file_base_name, i-1, 'mat'));
    if exist(traction_file_path, 'file')
        dummy = load(traction_file_path);
        tfm_fields{i} = dummy.tfm_field;
        clear dummy;
    end
end

end
