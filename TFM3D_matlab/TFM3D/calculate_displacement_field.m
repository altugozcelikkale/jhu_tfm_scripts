function calculate_displacement_field(TFM3D_directory, study_settings, app_config)
%%
volume_directory_path = app_config.get_volume_directory(TFM3D_directory);
displacement_directory_path = app_config.get_displacement_directory(TFM3D_directory);
make_directory(displacement_directory_path, 'overwrite');

coord_file_path = app_config.get_coord_mat_file_path(TFM3D_directory);
load(coord_file_path, 'coord');

Nx = coord.image_metadata.Nx;
Ny = coord.image_metadata.Ny;
Nz = coord.image_metadata.Nz;
F  = coord.F;
Po = coord.Po;
ds = study_settings.dvc.outputMeshSpacing;

% Displacement field data returned by DVC.
dvc_field = struct(); 
% Calculate coordinates the only once, include in the output file for each time step.
% image coordinates
dvc_field.xi = linspace(0, Nx, Nx/ds+1);
dvc_field.yi = linspace(0, Ny, Ny/ds+1);
dvc_field.zi = linspace(0, Nz, Nz/ds+1);
% physical coordinates
cp = coord.image2physical({dvc_field.xi, dvc_field.yi, dvc_field.zi}, F, Po);
dvc_field.x = cp{1};
dvc_field.y = cp{2};
dvc_field.z = cp{3};
[dvc_field.X, dvc_field.Y, dvc_field.Z] = meshgrid(dvc_field.x, dvc_field.y, dvc_field.z);

range = study_settings.process_range;
count = 1;
timer = tic();
for t = range
    app_config.log('\n###\nProcessing time point: %2d. (%2d out of %2d time points)\n###\n', t, count, numel(range));
    reference_volume_path = fullfile(volume_directory_path, app_config.generate_numbered_file_name(app_config.volume_file_base_name, 0, 'mat'));
    deformed_volume_path  = fullfile(volume_directory_path, app_config.generate_numbered_file_name(app_config.volume_file_base_name, t, 'mat'));
    output_file_path      = fullfile(displacement_directory_path, app_config.generate_numbered_file_name(app_config.displacement_file_base_name, t, 'mat'));

    [dvc_field.ui, dvc_field.cc, dvc_field.dm] = runDVCvolumePair(reference_volume_path, deformed_volume_path, study_settings.dvc);
    
    if study_settings.perform_displacement_filtering
        app_config.log('\n###\nFiltering displacement field...\n###\n');
        deformed_traction_surface = deform_traction_surface(coord.traction_surface, dvc_field.ui{1}, {dvc_field.X, dvc_field.Y, dvc_field.Z});    
        switch study_settings.displacement_filter_method
            case 'smoothn'
                % first mark the regions outside the hydrogel domain as NaN
                c = [dvc_field.X(:) dvc_field.Y(:) dvc_field.Z(:)];
                outDomain = inpolyhedron(deformed_traction_surface, c);
                outDomain = reshape(outDomain, size(dvc_field.X));
                ui_w_nan = cell(3,1);
                for i = 1:3
                    ui_w_nan{i} = dvc_field.ui{1}{i};
                    ui_w_nan{i}(outDomain) = nan;
                end
                ui = smoothn(ui_w_nan);
                dvc_field.ui{1}(1:3) = ui;
            case 'median'
                for i = 1:3
                    dvc_field.ui{1}{i} = medfilt3(dvc_field.ui{1}{i}); % 3x3x3 filter is used by default.
                end
            otherwise
                error('The displacement filter method specified is unrecognized: %s. Please check the study settings.', study_settings.displacement_filter_method);
        end
    
    end
    
    
    if study_settings.perform_displacement_filtering
        switch study_settings.displacement_filter_method
            case 'median'
                
            otherwise
                
        end
    end
    
    dvc_field.u{1}  =  dvc_field.ui{1}{1} * F;
    dvc_field.u{2}  = -dvc_field.ui{1}{2} * F;
    dvc_field.u{3}  =  dvc_field.ui{1}{3} * F;
    dvc_field.u_mag = sqrt(dvc_field.u{1}.^2 + dvc_field.u{2}.^2 + dvc_field.u{3}.^2);
        
    close(findall(groot, 'Tag', 'TMWWaitbar')) % Close all progress bars.
    save(output_file_path, 'dvc_field');
    count = count + 1;
    
end
elapsedTime = toc(timer);
app_config.log('\n###\nTotal Processing Time for runDVC is : %f min.\n###\n', elapsedTime/60);

end

