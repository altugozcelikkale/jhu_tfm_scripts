function fv = display_surface_mesh( surface_mesh, outward_normals, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if ischar(surface_mesh) % it is a path
    fv = stlread(surface_mesh);
elseif isstruct(surface_mesh) % Assume it is a fv structure
    fv = surface_mesh;
end


%% Surface manipulation
if outward_normals
    [fv, are_faces_flipped] = unifyMeshNormals(fv, 'alignTo', 'out'); 
    if any(are_faces_flipped)
        warning('There were face normal(s) facing inward. The faces are now oriented to have outward normals');
    end
end

%% Vertices and normals
v = fv.vertices;
n = calculateSurfaceNormals(fv);

%% Plot
if ~isempty(varargin)
    hfig = varargin{1};
    figure(hfig);
else
    figure;
end
hold on;
    patch(fv,'FaceColor',       [0.8 0.8 1.0], ...
             'FaceAlpha', 0.5,...
             'EdgeColor',       'k',        ...
             'FaceLighting',    'flat',     ...
             'AmbientStrength', 0.15);
quiver3(v(:,1),v(:,2),v(:,3),n(:,1),n(:,2),n(:,3));
hold off;
axis equal;

end % function

