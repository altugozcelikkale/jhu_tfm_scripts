function traction_surface = deform_traction_surface(traction_surface, U, COORD)
%DEFORM_TRACTION_SURFACE Summary of this function goes here
%   Detailed explanation goes here
v = traction_surface.vertices;
dv = zeros(size(v));
dv(:,1) = interp3(COORD{1}, COORD{2}, COORD{3}, U{1}, v(:,1), v(:,2), v(:,3), 'cubic');
dv(:,2) = interp3(COORD{1}, COORD{2}, COORD{3}, U{2}, v(:,1), v(:,2), v(:,3), 'cubic');
dv(:,3) = interp3(COORD{1}, COORD{2}, COORD{3}, U{3}, v(:,1), v(:,2), v(:,3), 'cubic');
dv(isnan(dv)) = 0; % if extrapolated, assume no deformation.
traction_surface.vertices = v + dv;
end

