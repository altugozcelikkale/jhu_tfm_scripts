function calculate_coordinates(TFM3D_directory, app_config)
% CALCULATE_COORDINATES calculates the image and physical coordinates for
% internal gel domain and traction surface mesh. Includes spatial
% coordinates as well as time.

coord = struct();

dataset_directory = app_config.get_dataset_directory(TFM3D_directory);
image_file_path = fullfile(dataset_directory, app_config.image_file_name);
traction_surface_mesh_file_path = fullfile(dataset_directory, app_config.traction_surface_mesh_file_name);

% Image metadata
coord.image_metadata = get_image_metadata(image_file_path);
md = coord.image_metadata; % shorthand.

% Pixel coordinates (spatial index)
coord.xi = 1:md.Nx; coord.xi = coord.xi(:);
coord.yi = 1:md.Ny; coord.yi = coord.yi(:);
coord.zi = 1:md.Nz; coord.zi = coord.zi(:);
[coord.Xi, coord.Yi, coord.Zi] = meshgrid(coord.xi, coord.yi, coord.zi);

% Physical coordinates
% First make sure the image volume is isotropically scaled. This is
% essential for DVC.
f = [md.dx md.dy md.dz];
if max(abs(f-mean(f))) > 0.001 % allow for a minor error of ~1um through whole FOV.
    error('Image spatial calibration in different dimensions differ from each other by a nontrivial amount. Are you sure the image volumes are isotropically scaled?\nWill stop.');
else
    coord.F = mean(f); % spatial calibration factor % um/pixel
end

% Import the traction surface to calculate the origin in terms of channel
% centroid.
traction_surface = stlread(traction_surface_mesh_file_path);

% Sanity check. Is traction surface coordinates are properly scaled? Physical scaling i.e in um's is expected. 
% However, there is no rigorous way to test and see whether the traction surface
% coordinates are in pixels or ums. Instead, we will do an adhoc test.
% It is generally expected that the traction surface length spans the majority of
% the selected image volume. i.e L_traction_surface >= say, 80% *
% L_image_volume
channel_length_x = max(traction_surface.vertices(:,1)) - min(traction_surface.vertices(:,1));
roi_length_x = md.Nx .* coord.F;
assert(channel_length_x./roi_length_x > 0.80, 'The traction surface does not seem to span across the imaging volume (<80%% coverage in x-direction). Are you sure the mesh was generated from an image calibrated in physical units?');

coord.Po = mean(traction_surface.vertices,1); % origin in physical coordinates;



coord.image2physical = @(ci, F, Po)({F * ci{1} - Po(1), ...
                                     Po(2) - F * ci{2}, ...
                                     F * ci{3} - Po(3)});
% TODO. add here the reverse operation
% coord.physical2image = @ ...

                                 
ci = {coord.xi, coord.yi, coord.zi};                                 
cp = coord.image2physical(ci, coord.F, coord.Po);

coord.x = cp{1};
coord.y = cp{2};
coord.z = cp{3};
[coord.X, coord.Y, coord.Z] = meshgrid(coord.x, coord.y, coord.z);

% Transform the coordinates of the traction surface vertices too.
cvi = {traction_surface.vertices(:,1), ...
       traction_surface.vertices(:,2), ...
       traction_surface.vertices(:,3)};
cvp = coord.image2physical(cvi, 1, coord.Po);

traction_surface.vertices = [cvp{1} cvp{2} cvp{3}];

% Finally, make sure the faces are oriented to have outward normals:
traction_surface = unifyMeshNormals(traction_surface, 'alignTo', 'out'); 
% Note that we need to do above last since previous coordinate manipulation, 
% like reversing y-coordinates has the effect of flipping the normals in opposite direction!

coord.traction_surface   = traction_surface;

% Time index and physical time
coord.ti = 1:md.Nt; coord.ti = coord.ti(:);
coord.time = (coord.ti-1) .* md.dt;

% Return coord. Also save the coordinates data for future use and reference.
coord_file_path = fullfile(TFM3D_directory, [app_config.coord_file_base_name '.mat']);
save(coord_file_path, 'coord');

end

