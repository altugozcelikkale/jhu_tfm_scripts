function calculate_traction_stresses(TFM3D_directory, study_settings, app_config)

displacement_directory_path = app_config.get_displacement_directory(TFM3D_directory);
traction_directory_path = app_config.get_traction_directory(TFM3D_directory);
make_directory(traction_directory_path, 'overwrite');

coord_file_path = app_config.get_coord_mat_file_path(TFM3D_directory);
load(coord_file_path, 'coord');

range = study_settings.process_range;
count = 1;
timer = tic();
for t = range
    t_timer = tic;
    app_config.log('TFM stress estimation for time point %2d (%2d out of %2d time points) ...', t, count, numel(range));
    displacement_file_path = fullfile(displacement_directory_path, app_config.generate_numbered_file_name(app_config.displacement_file_base_name, t, 'mat'));
    traction_file_path     = fullfile(traction_directory_path, app_config.generate_numbered_file_name(app_config.traction_file_base_name, t, 'mat'));
    
    load(displacement_file_path, 'dvc_field');
    u = dvc_field.u;
    deformed_traction_surface = deform_traction_surface(coord.traction_surface, u, {dvc_field.X, dvc_field.Y, dvc_field.Z});
    
    tfm_field = struct();
    tfm_field.x = dvc_field.x;
    tfm_field.y = dvc_field.y;
    tfm_field.z = dvc_field.z;
    tfm_field.X = dvc_field.X;
    tfm_field.Y = dvc_field.Y;
    tfm_field.Z = dvc_field.Z;
    tfm_field.deformed_traction_surface = deformed_traction_surface;
    
    [tfm_field.U, tfm_field.EIJ, tfm_field.EV, tfm_field.SIJ, tfm_field.TI, tfm_field.TnI, tfm_field.TsI] = ...
        calculateDeformationField(tfm_field.X, tfm_field.Y, tfm_field.Z, u, deformed_traction_surface, study_settings.tfm.E, study_settings.tfm.nu, study_settings.tfm.material_model);
    
    save(traction_file_path, 'tfm_field');
    
    app_config.log(' DONE in %.1f seconds.\n', toc(t_timer));
    
    count = count + 1;
    
end
elapsedTime = toc(timer);
app_config.log('\n###\nTotal Processing Time is : %f min.\n###\n', elapsedTime/60);
end