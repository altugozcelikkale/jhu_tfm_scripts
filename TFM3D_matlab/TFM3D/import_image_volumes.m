function import_image_volumes(TFM3D_directory, app_config)

dataset_directory = app_config.get_dataset_directory(TFM3D_directory);
volume_directory  = app_config.get_volume_directory(TFM3D_directory);


image_file_path = fullfile(dataset_directory, app_config.image_file_name);


%% Import Image
im = read_multidimensional_image(image_file_path);
% Requires bioformats. However, we may not want bioformats dependency for a task doable by tools
% in our hand.
% Volume image dimensions in voxels and time index
[~, ~, Nc, ~, Nt] = size(im);
% This is supposed to be single channel image
assert(Nc == 1, 'A single channel image stack is expected');
assert(Nt > 1, 'It seems the image has only one time point. At least 2 time points are necessary for DVC');

%% Save image stack as a volume at each time point
app_config.log('\n>> Processing images at %d time points:\n', Nt);
for i=1:Nt
    vol{1}=squeeze(im(:,:,1,:,i));
    volume_file_name = app_config.generate_numbered_file_name(app_config.volume_file_base_name, i-1, 'mat');
    save(fullfile(volume_directory, volume_file_name), 'vol');
    app_config.log('Time point %d/%d saved as file: %s.\n', i, Nt, volume_file_name);
end
%% Finalize:
 app_config.log('\n>> Image volume import complete.\n');
 
end %function
