clear;
clc;
close all;

init();
global FD GSET;

%% Material Parameters:
% Elastic Modulus
E = 26000; % [Pa]
% Poisson's Ratio
nu = 0.48; % [1]
% Material Model
materialModel = 'linearElastic'; %'neohookean'; vs 'linearElastic'

%% Study
study = FD.STUDIES('20170817_Field20_Channel2');
% Image information
% Calibration (um/pixel):
F = study.F;
% Origin (pixel coordinates)
Po = study.Po;

processingRange = 1:1;%(study.Nt-1); % Override here to limit the number of timesteps to process.
inputDir = fullfile(FD.GET_ANALYSIS_DIR(study.name), 'Displacement Field');
% Output Folder:
outputDir = fullfile(FD.GET_ANALYSIS_DIR(study.name), 'calculateTractionStresses');
if ~exist(outputDir, 'file')
    mkdir(outputDir)
end
% Also load the surface on which traction stresses will be calculated:
tractionSurface = stlread(fullfile(study.channelBoundaryFilePath));
%load(fullfile('Models', 'internalBoundary.mat'));
% Convert to physical coordinates:
v = tractionSurface.vertices;
scaleFactor = 1.0;
for i = 1:3
    v(:,i) = v(:,i) - F(i) * Po(i);
    center = mean(v(:,1));
    v(:,i) = scaleFactor * (v(:,i)-center) + center;
end
tractionSurface.vertices = v;

%% Make sure the normals of internalBoundary point outward
% tractionSurface = unifyMeshNormals(tractionSurface, 'alignTo', 'out');

% internalBoundary.faces = fliplr(internalBoundary.faces); % ensure outward normals.
%%
figure;
    patch(tractionSurface,'FaceColor',       [0.8 0.8 1.0], ...
             'EdgeColor',       'none',        ...
             'FaceLighting',    'gouraud',     ...
             'AmbientStrength', 0.15);
hold on;
v = tractionSurface.vertices;
n = calculateSurfaceNormals(tractionSurface);
quiver3(v(:,1),v(:,2),v(:,3),n(:,1),n(:,2),n(:,3), 0.5, 'r');
hold off;
axis equal;

%%
timer = tic();
count = 1;

for t = processingRange
    fprintf('\n###\nProcessing time point: %2d. (%2d out of %2d time points)\n###\n', t, count, numel(processingRange));
    % Stress Estimation
    inputFilePath      = fullfile(inputDir, FD.MAKE_FILE_NAME('postProcessDisplacement', t, 'mat'));
    DF = load(inputFilePath);
    X = DF.Xp;
    Y = DF.Yp;
    Z = DF.Zp;
    
    u = {DF.ux, DF.uy, DF.uz};
    [U, EIJ, EV, SIJ, TnI, TsI] = calculateDeformationField(X, Y, Z, u, tractionSurface, E, nu, 'linearElastic');
    
    outputFilePath      = fullfile(outputDir, FD.MAKE_FILE_NAME('TFM_Field', t, 'mat'));
    save(outputFilePath);
    outputFilePath      = fullfile(outputDir, FD.MAKE_FILE_NAME('TFM_Field_Domain', t, 'vtk'));
    vtkwrite(outputFilePath, 'structured_grid',X,Y,Z, ...
                                      'scalars', 'Ux', U{1}{1},...
                                      'scalars', 'Uy', U{1}{2},...
                                      'scalars', 'Uz', U{1}{3},...
                                      'scalars', 'EXX', EIJ{1}{1,1},...
                                      'scalars', 'EYY', EIJ{1}{2,2},...
                                      'scalars', 'EZZ', EIJ{1}{3,3},...
                                      'scalars', 'EXY', EIJ{1}{1,2},...
                                      'scalars', 'EXZ', EIJ{1}{1,3},...
                                      'scalars', 'EYZ', EIJ{1}{2,3},...
                                      'scalars', 'SXX', SIJ{1}{1,1},...
                                      'scalars', 'SYY', SIJ{1}{2,2},...
                                      'scalars', 'SZZ', SIJ{1}{3,3},...
                                      'scalars', 'SXY', SIJ{1}{1,2},...
                                      'scalars', 'SXZ', SIJ{1}{1,3},...
                                      'scalars', 'SYZ', SIJ{1}{2,3}...
                                      );
                                  
    outputFilePath      = fullfile(outputDir, FD.MAKE_FILE_NAME('TFM_Field_Surface', t, 'vtk'));
    v = tractionSurface.vertices;
    vtkwrite(outputFilePath, 'unstructured_grid',v(:,1), v(:,2), v(:,3), ...
                                      'scalars', 'Ux', U{2}{1},...
                                      'scalars', 'Uy', U{2}{2},...
                                      'scalars', 'Uz', U{2}{3},...
                                      'scalars', 'EXX', EIJ{2}{1,1},...
                                      'scalars', 'EYY', EIJ{2}{2,2},...
                                      'scalars', 'EZZ', EIJ{2}{3,3},...
                                      'scalars', 'EXY', EIJ{2}{1,2},...
                                      'scalars', 'EXZ', EIJ{2}{1,3},...
                                      'scalars', 'EYZ', EIJ{2}{2,3},...
                                      'scalars', 'SXX', SIJ{2}{1,1},...
                                      'scalars', 'SYY', SIJ{2}{2,2},...
                                      'scalars', 'SZZ', SIJ{2}{3,3},...
                                      'scalars', 'SXY', SIJ{2}{1,2},...
                                      'scalars', 'SXZ', SIJ{2}{1,3},...
                                      'scalars', 'SYZ', SIJ{2}{2,3}...
                                      );
        
    count = count + 1;
end
elapsedTime = toc(timer);
fprintf('\n###\nTotal Processing Time for calculateTractionStresses is : %f min.\n###\n', elapsedTime/60);

%%



figure();
Tnmag = sqrt(TnI{1}.^2 + TnI{2}.^2 + TnI{3}.^2);
Tsmag = sqrt(TsI{1}.^2 + TsI{2}.^2 + TsI{3}.^2);
Tmag  = sqrt(Tnmag.^2 + Tsmag.^2);
Sij = SIJ{2};
var = Sij{2,2};
hold on;
v = tractionSurface.vertices; 
h = patch(tractionSurface, 'FaceVertexCData',Tnmag);
%quiver3(v(:,1), v(:,2), v(:,3), TnI{1}, TnI{2}, TnI{3}, 1, 'k');
%quiver3(v(:,1), v(:,2), v(:,3), TsI{1}, TsI{2}, TsI{3}, 1, 'k');
hold off;
colormap(jet(256));
colorbar;
Xmin = min(X(:));
Ymin = min(Y(:));
Zmin = min(Z(:));
Xmax = max(X(:));
Ymax = max(Y(:));
Zmax = max(Z(:));
Cmin =0;
Cmax =50000;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'FaceAlpha', 1, ...
    'DiffuseStrength',.8);

set(gca, ...
        'XLim'         , [Xmin Xmax], ...
        'YLim'         , [Ymin Ymax], ...
        'ZLim'         , [Zmin Zmax], ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(-30,25);
title('Normal Traction Stress Magnitude');