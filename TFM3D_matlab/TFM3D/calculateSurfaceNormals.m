function N = calculateSurfaceNormals(FV)
%GETSURFACENORMALS Wrapper around patchnormals, but provides the normals
%based on right hand rule.
N = -patchnormals(FV); % Simple inversion is enough.
% TODO: This is a hacky way to do this. Does this work every time?
end

