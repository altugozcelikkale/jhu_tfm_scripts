function [du, varargout] = calculateGradient(u, X, Y, Z, internalBoundary, varargin)
%CALCULATEGRADIENT calculate gradient of scalar valued function of 3 dimensions in the
%presence of internal boundaries
%   Central differencing within the domain. 
%   Ghost cell method combined with algebraic polynomial extrapolation
%   normal to the boundary at the internal boundary.

%% Flags
DEBUG = false;

% Default values for algorithm settings:
defaultSettings.distanceFactor = 0.75; % empirically determined for the channel geometry data.
defaultSettings.isAbsoluteFactor = false;
if isempty(varargin)
    settings = defaultSettings;
elseif length(varargin) == 1
    settings = varargin{1};
end

% Spacings (assumed to be uniform)
dx = X(1,2,1)-X(1,1,1);
dy = Y(2,1,1)-Y(1,1,1);
dz = Z(1,1,2)-Z(1,1,1);


%% Find out of domain region (enclosed in internal boundary)
coord = [X(:) Y(:) Z(:)];
outDomain = inpolyhedron(internalBoundary, coord);
outDomain = reshape(outDomain, size(X));
%% Extract ghost cells
% for i = 1:1
%     outDomain = imerode(outDomain, true(3));
% end
isGhostCell = boolean(outDomain - imerode(outDomain, strel('cube', 3)));
ghostCoord = [X(isGhostCell) Y(isGhostCell) Z(isGhostCell)];

%% Trace ghost cells to and the internal boundary surface and into the domain based on surface normal.
[distances, surfaceCoord] = point2trimesh(internalBoundary, 'QueryPoints', ghostCoord, 'Algorithm', 'parallel');
normals = (ghostCoord - surfaceCoord)./distances(:); % distances are negative when coming from the inside of the outdomain

% Distances between evaluation points for construction of extrapolation
% polynomial
if settings.isAbsoluteFactor
    ds = settings.distanceFactor;
else
    ds = settings.distanceFactor*sqrt(dx^2 + dy^2 + dz^2);
end

% Evaluation point coordinates
p1 = surfaceCoord + normals .* ds;
p2 = surfaceCoord + normals .* 2 .* ds;
p3 = surfaceCoord + normals .* 3 .* ds;
l  = sqrt(sum((ghostCoord - p1).^2, 2));

%% Construct interpolant for extrapolation into out-of-domain region:
uintop = scatteredInterpolant(X(~outDomain), Y(~outDomain), Z(~outDomain), u(~outDomain), 'linear', 'linear');

% Evaluate extrapolated ghost cell values based on 2. degree Lagrange
% interpolant passing through p1,p2,p3.
u1 = uintop(p1);
u2 = uintop(p2);
u3 = uintop(p3);
ug = u1 + ((u3 + u1 - 2.* u2) .*l.^2 - (4 .* u2 - 3.* u1 - u3) .* l .* ds) ./2 ./ds.^2;
uint = uintop(X,Y,Z);
uint(outDomain) = 0;
uint(isGhostCell) = ug; 

%% Differentiation:
du = {};
[du{1}, du{2}, du{3}] = gradient(uint, dx, dy, dz);

%% Built another interpolant for the gradient components:
duintop = uintop;
for i = 1:3
    f = du{i};
    duintop.Values = f(~outDomain);
    du1 = duintop(p1);
    du2 = duintop(p2);
    du3 = duintop(p3);
    dug = du1 + ((du3 + du1 - 2.* du2) .*l.^2 - (4 .* du2 - 3.* du1 - du3) .* l .* ds) ./2 ./ds.^2;
    duint = duintop(X,Y,Z);
    duint(outDomain) = 0;
    duint(isGhostCell) = dug;
    du{i} = duint;
end

%% Evaluate gradients at the domain boundary points
interpolationMethod = 'linear';
boundaryPoints = internalBoundary.vertices;

ub = interp3(X,Y,Z,uint,boundaryPoints(:,1),boundaryPoints(:,2),boundaryPoints(:,3), interpolationMethod, nan);
% Any nan values will be filled by nearest neighbor interpolation.
ub = fill_missing_values_on_3D_surface(ub, boundaryPoints);
dub = cell(3,1);
for i=1:3
    dub{i} = interp3(X,Y,Z,du{i},boundaryPoints(:,1),boundaryPoints(:,2),boundaryPoints(:,3), interpolationMethod, nan);
    % Any nan values will be filled by nearest neighbor interpolation.
    dub{i} = fill_missing_values_on_3D_surface(dub{i}, boundaryPoints);
end
%% Mask out-of-domain values
ui = uint;
ui(outDomain)=nan;
for i=1:3
    var = du{i};
    var(outDomain)=nan;
    du{i} = var;
end

%% Output
% du;  gradient of in-domain points (nan for out-of-domain)
% Optionally return:
varargout{1} = dub; % gradient at the domain boundary (evaluated at internalBoundary vertices)
varargout{2} = ui;  % function values at the in-domain points (nan for out-of-domain).
varargout{3} = ub;  % function values at the domain boundary since this is not retrievable from u.

%% Debug mode
if DEBUG
    % Out-of-domain region bounded by internalBoundary
    figure();
    fv = isosurface(X,Y,Z,outDomainTemp,0.99);
    fv.faces = fliplr(fv.faces);
    patch(fv,'FaceColor',       [0.8 0.8 1.0], ...
             'EdgeColor',       'none',        ...
             'FaceLighting',    'gouraud',     ...
             'AmbientStrength', 0.15);

    % Add a camera light, and tone down the specular highlighting
    camlight('headlight');
    material('dull');

    % Fix the axes scaling, and set a nice view angle
    axis('image');
    view([-135 35]);
    
    figure();
    h = slice(X, Y, Z, u, 0, 0, 0);
    colorbar;
    Cmin = 0;
    Cmax =1;
    set(h,'FaceColor','interp',...
        'EdgeColor','none',...
        'DiffuseStrength',.8);

    set(gca, ...
            'CLim'         , [Cmin Cmax] ...
            )
    daspect([1 1 1]);
    view(0,90);
    title('u');
    
    figure();
    h = slice(X, Y, Z, ui ,0, 0, 0);
    colorbar;
    Cmin = 0;
    Cmax =1;
    set(h,'FaceColor','interp',...
        'EdgeColor','none',...
        'DiffuseStrength',.8);

    set(gca, ...
            'CLim'         , [Cmin Cmax] ...
            )
    daspect([1 1 1]);
    view(0,90);
    title('u_{int}');
    
    figure();
    h = quiver3(ghostCoord(:,1), ghostCoord(:,2), ghostCoord(:,3), normals(:,1), normals(:,2), normals(:,3));
%     colorbar;
%     Cmin = 0;
%     Cmax =1;
%     set(h,'FaceColor','interp',...
%         'EdgeColor','none',...
%         'DiffuseStrength',.8);
% 
%     set(gca, ...
%             'CLim'         , [Cmin Cmax] ...
%             )
    daspect([1 1 1]);
    view(0,90);
    title('Ghost Cell Normals');


end

