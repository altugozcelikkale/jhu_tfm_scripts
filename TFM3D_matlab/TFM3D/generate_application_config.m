function config = generate_application_config(varargin)
% GENERATE_APPLICATION_CONFIG generates and returns an application configuration structure that holds predefined directories, file 
% names, common settings, and dependencies. These variables are accessed
% and used by rest of the application.
config = struct();

% Check whether Gui is running or not
config.gui_tag = 'TFM_gui';
config.is_gui_running = @()(~isempty(findall(groot, 'Tag', config.gui_tag)));

if isempty(varargin)
    config.is_gui = false;
else
    config.is_gui = varargin{1};
end
%% (F)iles and (D)irectory layout
config.TFM3D_base_directory_name = 'TFM3D'; % Create with this name if does not exist. Don't rely on that for existing folders. The folder may have been renamed for backups.

% The dataset folder is always one level above the TFM3D process directory.
% This is hardcoded. 
config.get_parent_directory      = @(directory_path)(fullfile(directory_path, '..')); % Generic func.
config.get_dataset_directory     = config.get_parent_directory;
% ... and other way around.
config.get_TFM3D_base_directory  = @(dataset_directory)(fullfile(dataset_directory, config.TFM3D_base_directory_name));

% Dataset folder
config.image_file_name = 'TFM_roi_c1.tif';
config.traction_surface_mesh_file_name = 'channel_wall_smoothed.stl';
config.dataset_info_file_name = 'dataset_info.xlsx';
config.TFM3D_dataset_identifier_file_name = config.dataset_info_file_name; % Existence of this file is checked when searching for TFM3D folders during batch processing. If file is found, the folder is considered to represent a TFM3D dataset.


% TFM3D process directory
config.volume_directory_rel_path = 'volumes';
config.displacement_directory_rel_path = 'displacement field';
config.traction_directory_rel_path = 'traction stress field';
config.postprocess_directory_rel_path = 'postprocess'; % We should everything after traction stress calculation in this folder.
config.vtk_exports_directory_rel_path = fullfile(config.postprocess_directory_rel_path, 'vtk exports');
config.summary_directory_rel_path = fullfile(config.postprocess_directory_rel_path, 'summary');


% Various mat files recording the application metadata and analysis data
config.TFM3D_mat_file_name = 'TFM3D.mat'; % This is hardcoded. A fixed file name is necessary so that subsequent postprocess scripts can independently find the TFM3D base directory based on existence of this file.
config.volume_file_base_name = 'vol';
config.coord_file_base_name = 'coord'; % coordinates
config.displacement_file_base_name = 'displacement_field';
config.traction_file_base_name = 'traction_stress_field';
config.vtk_hydrogel_domain_file_base_name = 'hydrogel_domain';
config.vtk_channel_wall_file_base_name = 'channel_wall';
config.summary_file_base_name = 'summary';


%Utility functions
 % Get directories
config.get_volume_directory        = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.volume_directory_rel_path));
config.get_displacement_directory  = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.displacement_directory_rel_path));
config.get_traction_directory      = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.traction_directory_rel_path));
config.get_postprocess_directory   = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.postprocess_directory_rel_path));
config.get_vtk_exports_directory   = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.vtk_exports_directory_rel_path));
config.get_summary_directory       = @(TFM3D_base_directory_path)(fullfile(TFM3D_base_directory_path, config.summary_directory_rel_path));


% Get specific files
config.generate_numbered_file_name = @(base_name, iterator, extension)(sprintf('%s_%02d.%s', base_name, iterator, extension));
config.get_TFM3D_mat_file_path     = @(TFM3D_directory)(fullfile(TFM3D_directory, config.TFM3D_mat_file_name)); % We could do without this. But, good to have as a convenience.
config.get_coord_mat_file_path     = @(TFM3D_directory)(fullfile(TFM3D_directory, [config.coord_file_base_name '.mat']));

config.make_directory_option = 'do_nothing';%'backup'; % Other possible values: 'overwrite', 'backup' (recommended), 'do_nothing' (not recommended)

% Process states
% An enum class would be ideal, but we would like to maintain portability.
% Users may want to inspect the data without having access to the TFM3D software.
% A rudimentary state enumeration with a structure
possible_process_states = {'INITIALIZE', 'IMPORT_DATASET', 'DVC', 'TFM', 'POSTPROCESS', 'COMPLETE'};
config.process_states = struct();
for i = 1:length(possible_process_states)
    config.process_states.(possible_process_states{i}) = i;
end
config.get_process_state_str = @(ps)(possible_process_states{ps});
%% Input and Output Logging
if config.is_gui
    %TODO
else % command line batch execution
    config.log = @(formatted_str, varargin)(fprintf(formatted_str, varargin{:}));
end
    
end % function



