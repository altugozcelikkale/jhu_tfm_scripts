function [tfm_fields] = export_fields_to_vtk(TFM_directory, tfm_fields, app_config)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

vtk_exports_directory = app_config.get_vtk_exports_directory(TFM_directory);
make_directory(vtk_exports_directory, 'overwrite');

for i = 1:length(tfm_fields)   
    tfm_field = tfm_fields{i};
    if ~isempty(tfm_field)    
        %% Export fields defined in hydrogel domain
        mesh2ndgrid = @(x)(permute(x, [2,1,3])); 
        grid_info = struct();
        grid_info.type = 'structured_grid';
        grid_info.label = app_config.vtk_hydrogel_domain_file_base_name;
        Xvtk = mesh2ndgrid(tfm_field.X);
        Yvtk = mesh2ndgrid(tfm_field.Y);
        Zvtk = mesh2ndgrid(tfm_field.Z);
        grid_info.dimensions = size(Xvtk);
        grid_info.points = [Xvtk(:) Yvtk(:) Zvtk(:)];
     
        datasets = {};

        data = struct();
        data.type = 'vector';
        data.label = 'U';
        data.values = [reshape(mesh2ndgrid(tfm_field.U{1}{1}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.U{1}{2}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.U{1}{3}), [], 1)];
        data.values(isnan(data.values)) = 0; % Paraview can't handle NaNs! :(
        datasets{end+1} = data;
        
        Umag = sqrt(tfm_field.U{1}{1}.^2 + tfm_field.U{1}{2}.^2 + tfm_field.U{1}{3}.^2);
        
        data = struct();
        data.type = 'scalar';
        data.label = 'Umag';
        data.values = reshape(mesh2ndgrid(Umag), [], 1);
        data.values(isnan(data.values)) = 0;
        datasets{end+1} = data;
        
        data = struct();
        data.type = 'scalar';
        data.label = 'EV';
        data.values = reshape(mesh2ndgrid(tfm_field.EV{1}), [], 1);
        data.values(isnan(data.values)) = 0;
        datasets{end+1} = data;
        
        vtk_output_file_path = fullfile(vtk_exports_directory, app_config.generate_numbered_file_name(app_config.vtk_hydrogel_domain_file_base_name, i-1, 'vtk'));
        write_vtk(vtk_output_file_path, grid_info, datasets);

        
        %% Export fields defined on traction surface i.e. hydrogel_surface
        grid_info = struct();
        grid_info.type = 'unstructured_grid';
        grid_info.label = app_config.vtk_channel_wall_file_base_name;
        grid_info.points = tfm_field.deformed_traction_surface.vertices;
        grid_info.cells = tfm_field.deformed_traction_surface.faces;

        datasets = {};
        data = struct();
        data.type = 'vector';
        data.label = 'U';
        data.values = [reshape(mesh2ndgrid(tfm_field.U{2}{1}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.U{2}{2}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.U{2}{3}), [], 1)];
        datasets{end+1} = data;
        
        Umag = sqrt(tfm_field.U{2}{1}.^2 + tfm_field.U{2}{2}.^2 + tfm_field.U{2}{3}.^2);
        
        data = struct();
        data.type = 'scalar';
        data.label = 'Umag';
        data.values = reshape(mesh2ndgrid(Umag), [], 1);
        datasets{end+1} = data;
        
        data = struct();
        data.type = 'scalar';
        data.label = 'EV';
        data.values = reshape(mesh2ndgrid(tfm_field.EV{2}), [], 1);
        datasets{end+1} = data;
        
        data = struct();
        data.type = 'vector';
        data.label = 'Tn';
        data.values = [reshape(mesh2ndgrid(tfm_field.TnI{1}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TnI{2}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TnI{3}), [], 1)];
        datasets{end+1} = data;
        Tnmag = sqrt(tfm_field.TnI{1}.^2 + tfm_field.TnI{2}.^2 + tfm_field.TnI{3}.^2);
        
        data = struct();
        data.type = 'scalar';
        data.label = 'Tnmag';
        data.values = reshape(mesh2ndgrid(Tnmag), [], 1);
        datasets{end+1} = data;
        
        data = struct();
        data.type = 'vector';
        data.label = 'Ts';
        data.values = [reshape(mesh2ndgrid(tfm_field.TsI{1}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TsI{2}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TsI{3}), [], 1)];
        datasets{end+1} = data;
        
        Tsmag = sqrt(tfm_field.TsI{1}.^2 + tfm_field.TsI{2}.^2 + tfm_field.TsI{3}.^2);
        
        data = struct();
        data.type = 'scalar';
        data.label = 'Tsmag';
        data.values = reshape(mesh2ndgrid(Tsmag), [], 1);
        datasets{end+1} = data;
        
                data = struct();
        data.type = 'vector';
        data.label = 'T';
        data.values = [reshape(mesh2ndgrid(tfm_field.TI{1}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TI{2}), [], 1), ...
                       reshape(mesh2ndgrid(tfm_field.TI{3}), [], 1)];
        datasets{end+1} = data;
        Tmag = sqrt(tfm_field.TI{1}.^2 + tfm_field.TI{2}.^2 + tfm_field.TI{3}.^2);
        
        data = struct();
        data.type = 'scalar';
        data.label = 'Tmag';
        data.values = reshape(mesh2ndgrid(Tmag), [], 1);
        datasets{end+1} = data;
        
        
        
        vtk_output_file_path = fullfile(vtk_exports_directory, app_config.generate_numbered_file_name(app_config.vtk_channel_wall_file_base_name, i-1, 'vtk'));
        write_vtk(vtk_output_file_path, grid_info, datasets);
    end
end


end % function