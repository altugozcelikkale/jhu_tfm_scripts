function varargout = runDVCvolumePair(referenceVolumePath, deformedVolumePath, DVCsettings)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

volumeFileStr = 'vol_%02d.mat';

workDirectory = fullfile(pwd, 'temp', 'runDVC_work');
if exist(workDirectory, 'file')
    rmdir(workDirectory, 's');
end
mkdir(workDirectory);


%% Copy files into the temporary work directory.
copyfile(referenceVolumePath, fullfile(workDirectory, sprintf(volumeFileStr,0)));
copyfile(deformedVolumePath,  fullfile(workDirectory, sprintf(volumeFileStr,1)));
filename = fullfile(workDirectory, 'vol_*.mat');

% Turn on parallel computing: TODO: extend this to support concurrent
% executions
% setParallelComputing(true, 4);
% loopTimer = tic;
    
%% Estimate displacements via IDVC
[u, cc, dm] = funIDVC(filename, DVCsettings.interrogationWindowSize, DVCsettings.incORcum, DVCsettings);
 varargout{1} = u;
 varargout{2} = cc;
 varargout{3} = dm;
 
%% Cleanup:
 rmdir(workDirectory, 's')

end
