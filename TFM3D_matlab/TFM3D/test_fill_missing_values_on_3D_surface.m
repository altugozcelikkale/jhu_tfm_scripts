clear;
clc;
close all;

surface_mesh_file_path = 'T:\Work\JHU\Imaging\3D_TFM_Dataset_20180314\012418_12kPa\field2\channel2\channel_wall_smoothed.stl';
surface_mesh = display_surface_mesh(surface_mesh_file_path, true);
u = surface_mesh.vertices(:,1) + surface_mesh.vertices(:,2) + surface_mesh.vertices(:,3);
u = u + 5*(rand(size(u)) - rand(size(u)));
N = numel(u);
Nnan = floor(0.20*N);
u(randperm(N,Nnan)) = nan;

%% Plot
figure;
    patch(surface_mesh,'FaceVertexCData',u, ...
             'FaceColor',       'interp', ...
             'FaceAlpha', 0.5,...
             'EdgeColor',       'None',        ...
             'FaceLighting',    'flat',     ...
             'AmbientStrength', 0.15);
axis equal;
title('Surface field variable with NaNs');

ufilled = fill_missing_values_on_3D_surface(u, surface_mesh.vertices);
figure;
    patch(surface_mesh,'FaceVertexCData',ufilled, ...
             'FaceColor',       'interp', ...
             'FaceAlpha', 0.5,...
             'EdgeColor',       'None',        ...
             'FaceLighting',    'flat',     ...
             'AmbientStrength', 0.15);
axis equal;
title('Surface field variable without NaNs');

sum(isnan(u))
sum(isnan(ufilled))
