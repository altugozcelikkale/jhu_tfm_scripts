function [TFM3D_directory, dataset_info, study_settings] = initialize_TFM3D_study(dataset_directory, app_config)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here


% TODO add sanity check for required dataset files. i.e. check and see whether the
% designated image, dataset info as well as the traction surface mesh
% exists.

TFM3D_directory = app_config.get_TFM3D_base_directory(dataset_directory);
make_directory(TFM3D_directory, app_config.make_directory_option);

volume_directory_path = app_config.get_volume_directory(TFM3D_directory);
make_directory(volume_directory_path, 'overwrite');

dataset_info_file_path = fullfile(app_config.get_dataset_directory(TFM3D_directory), app_config.dataset_info_file_name);
dataset_info = table2struct(readtable(dataset_info_file_path));

image_file_path = fullfile(app_config.get_dataset_directory(TFM3D_directory), app_config.image_file_name);
image_metadata = get_image_metadata(image_file_path);

study_settings = generate_default_study_settings();
% Complete study settings that depend on dataset specifics.
if isfield('process_range_str', dataset_info)
    study_settings.process_range_str = char(dataset_info.process_range_str);
else
    study_settings.process_range_str = sprintf('0:%d', image_metadata.Nt-1);
end
study_settings.process_range = eval(study_settings.process_range_str);
study_settings.tfm.E = dataset_info.elastic_modulus;

% Write down the current process state:
process_state = app_config.process_states.INITIALIZE;
save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);

end

