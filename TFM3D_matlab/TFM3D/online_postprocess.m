function online_postprocess(TFM3D_directory, dataset_info, study_settings, app_config)
%ONLINE_POSTPROCESS A generic function to run any postprocessing steps that
%are to be performed immediately after TFM field estimation. Additional postprocessing can be performed offline using a separate
% script.

% Detailed explanation goes here
tfm_fields = get_fields(TFM3D_directory, app_config);
export_fields_to_vtk(TFM3D_directory, tfm_fields, app_config);

% Write down the current process state:
process_state = app_config.process_states.POSTPROCESS;
save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);

end

