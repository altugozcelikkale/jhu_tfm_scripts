function save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
% Save file:
file_path = app_config.get_TFM3D_mat_file_path(TFM3D_directory);
save(file_path , 'process_state', 'dataset_info', 'study_settings', 'app_config');
end

