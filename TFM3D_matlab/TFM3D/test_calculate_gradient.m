clc;
close all;

u = dvc_field.u{1};
X = dvc_field.X;
Y = dvc_field.Y;
Z = dvc_field.Z;
traction_surface = coord.traction_surface;
traction_surface = unifyMeshNormals(traction_surface, 'alignTo', 'out');
traction_surface.vertices(:,2) = -traction_surface.vertices(:,2); %flip
[dui, dub, ui, ub] = calculateGradient(u, X, Y, Z, traction_surface);

%%
hfig = figure;
[Ny, Nx, Nz] = size(dvc_field.X);
sx= 0;-25:25:25;
sy= 0;-5:5:5;
sz= 0;-5:5:5;
V = ui;
slice(dvc_field.X, dvc_field.Y, dvc_field.Z, V, sx,sy,sz);
axis equal;

display_surface_mesh(traction_surface, true, hfig);


%%
traction_surface = coord.traction_surface;

