function u = fill_missing_values_on_3D_surface(u, vertices)
%FILL_MISSING_VALUES_ON_3D_SURFACE interpolates nan values in u using
%nearest neighbor interpolation on a 3d surface having vertices.
i_nan = find(isnan(u));
for i = 1:numel(i_nan)
    v = vertices(i_nan(i));
    valid_vertices = vertices(~isnan(u),:);
    valid_u = u(~isnan(u));
    distance_vec = bsxfun(@minus, valid_vertices, v); % let's maintain backward compatibility. Implicit expansion available Matlab2016+ only.
    distances = sqrt(sum(distance_vec.*distance_vec, 2));
    [~, i_min] = min(distances);
    u(i_nan(i)) = valid_u(i_min);
end

