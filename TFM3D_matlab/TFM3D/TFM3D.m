%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main script for batch processing with TFM3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Clean workspace
clear;
clc;
close all;

%% Application configuration
app_config = generate_application_config();
date_str = 'dd-mmm-yyyy HH:MM:SS';
%% Identify the TFM datasets to be processed
enable_GUI_selection = true;

if enable_GUI_selection
    base_dir = uigetdir('', 'Please select the base folder to search for TFM3D datasets');
    if base_dir == 0
        fprintf('The dialog was cancelled. Stopping here.\n')
        return;
    end
    
    fprintf('\nSearching the base folder and its subfolders for TFM3D datasets... ');
    dataset_base_dirs = find_datasets(base_dir, app_config.TFM3D_dataset_identifier_file_name);
    fprintf('DONE\n');
    
else
    dataset_base_dirs = 'A cell array of dataset directories.';
end

%%

Nds = length(dataset_base_dirs);
fprintf('\n<<TFM3D>> %s: %d datasets will be processed\n',datestr(now, date_str), Nds);
batch_process_timer = tic();
for i = 1:Nds
    
    dataset_directory = dataset_base_dirs{i};
    fprintf('<<TFM3D>> Dataset %d/%d: ...%s\n', i, Nds, dataset_directory(max(1, end-64):end));
    dataset_timer = tic();
    
    %% INITIALIZATION
    [TFM3D_directory, dataset_info, study_settings] = initialize_TFM3D_study(dataset_directory, app_config);
    
    %% DATA IMPORT
    import_image_volumes(TFM3D_directory, app_config);
    calculate_coordinates(TFM3D_directory, app_config);
    
    % Write down the current process state:
    process_state = app_config.process_states.IMPORT_DATASET;
    save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);
    
    
    %% DISPLACEMENT ESTIMATION BY DVC
    fprintf('<<TFM3D>> Estimating Displacement Field ...\n');
    calculate_displacement_field(TFM3D_directory, study_settings, app_config);
    
    % Write down the current process state:
    process_state = app_config.process_states.DVC;
    save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);
    
    
    %% TRACTION FORCE ESTIMATION BY DIRECT TFM
    fprintf('<<TFM3D>> Estimating Traction Stresses ...\n');
    calculate_traction_stresses(TFM3D_directory, study_settings, app_config);
    
    % Write down the current process state:
    process_state = app_config.process_states.TFM;
    save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);
    
    
    %% Postprocess
    fprintf('<<TFM3D>> Postprocessing e.g vtk exports ...\n');
    online_postprocess(TFM3D_directory, dataset_info, study_settings, app_config)
    
    % Write down the current process state:
    process_state = app_config.process_states.POSTPROCESS;
    save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);
    
    
    %% Finalize
    % Write down the current process state:
    process_state = app_config.process_states.COMPLETE;
    save_TFM3D_state(TFM3D_directory, process_state, dataset_info, study_settings, app_config);
    
    fprintf('\n<<TFM3D>> Completed processing dataset %d/%d in %.2f minutes.\n', i, Nds, toc(dataset_timer)/60);

end

fprintf('\n<<TFM3D>> %s: Execution completed. Elapsed time is %.2f minutes.\n\n',datestr(now, date_str), toc(batch_process_timer)/60);

% end of script

