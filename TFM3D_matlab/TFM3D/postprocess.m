%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main script for batch processing with TFM3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Clean workspace
clear;
clc;
close all;

%% Application configuration
app_config = generate_application_config();
date_str = 'dd-mmm-yyyy HH:MM:SS';
%% Identify the TFM datasets to be processed
enable_GUI_selection = true;

if enable_GUI_selection
    base_dir = uigetdir('', 'Please select the base folder to search for TFM3D datasets');
    if base_dir == 0
        fprintf('The dialog was cancelled. Stopping here.\n')
        return;
    end
    
    fprintf('\nSearching the base folder and its subfolders for TFM3D datasets... ');
    dataset_base_dirs = find_datasets(base_dir, app_config.TFM3D_dataset_identifier_file_name);
    fprintf('DONE\n');
    
else
    dataset_base_dirs = 'A cell array of dataset directories.';
end

%%

Nds = length(dataset_base_dirs);
fprintf('\n<<TFM3D postprocess>> %s: %d datasets will be processed\n',datestr(now, date_str), Nds);
batch_process_timer = tic();
for i = 1:Nds
    
    dataset_directory = dataset_base_dirs{i};
    fprintf('\n<<TFM3D postprocess>> Processing dataset %d/%d: ...%s\n',i, Nds, dataset_directory(max(1, end-64):end));
    TFM3D_directory   = app_config.get_TFM3D_base_directory(dataset_directory);
    TFM3D             = load(app_config.get_TFM3D_mat_file_path(TFM3D_directory)); % Dataset specific settings stored here.
    
    % Make sure the dataset has been processed up to the end of `TFM` step:
    if TFM3D.process_state < app_config.process_states.TFM
        fprintf('<<TFM3D postprocess>> *** Ignore dataset: ...%s.\nTFM processing is not complete for this dataset.\n', dataset_directory(max(1, end-64):end));
        continue;
    end
     
    
    summary_directory_path = app_config.get_summary_directory(TFM3D_directory);
    make_directory(summary_directory_path, 'overwrite');
    
    tfm_fields = get_fields(TFM3D_directory, TFM3D.app_config);
    Nt = length(tfm_fields);
    % We are only interested in forces/stresses on channel wall surface:
    for j = Nt:-1:1
        sd = struct(); % summary data...
        % traction surface coordinates:
        sd.ts = tfm_fields{j}.deformed_traction_surface;
        
        % Traction components
        % These still contain NaN values where the traction stress could
        % not be evaluated. Remove those by interpolation.
        dummy = tfm_fields{j}.TI;
        for k = 1:3
            dummy{k} = fill_missing_values_on_3D_surface(dummy{k}, sd.ts.vertices);
        end
        sd.T = dummy; 
        
        dummy = tfm_fields{j}.TnI;
        for k = 1:3
            dummy{k} = fill_missing_values_on_3D_surface(dummy{k}, sd.ts.vertices);
        end
        sd.Tn = dummy;
        
        dummy = tfm_fields{j}.TsI;
        for k = 1:3
            dummy{k} = fill_missing_values_on_3D_surface(dummy{k}, sd.ts.vertices);
        end
        sd.Ts = dummy;
        
        calculate_magnitude = @(x)(sqrt(x{1}.^2 + x{2}.^2 + x{3}.^2));
        sd.Tmag = calculate_magnitude(sd.T);
        sd.Tnmag = calculate_magnitude(sd.Tn);
        sd.Tsmag = calculate_magnitude(sd.Ts);
        % TODO: Do traction filtering here e.g. remove out-of-cell regions
        % based on a threshold...
        
        
        % Descriptive statistics
        sd.T_stats = calculate_descriptive_statistics(sd.Tmag);
        sd.Tn_stats = calculate_descriptive_statistics(sd.Tnmag);
        sd.Ts_stats = calculate_descriptive_statistics(sd.Tsmag);
        summary_t(j) = sd;
        
        % Export
        % mat files 
        summary_file_name = app_config.generate_numbered_file_name(app_config.summary_file_base_name, j-1, 'mat');
        summary_file_path = fullfile(summary_directory_path, summary_file_name);
        save(summary_file_path, '-struct', 'sd');
        
    end
    
    % Also Export the summart stats for the complete dataset i.e. (average
    % of all time points). This will be used in calculating population
    % metrics.
    T_stats_all =table();
    Ts_stats_all =table();
    Tn_stats_all =table();
    for j=2:Nt 
        T_stats_all = [T_stats_all; summary_t(j).T_stats];
        Tn_stats_all = [Tn_stats_all; summary_t(j).Tn_stats];
        Ts_stats_all = [Ts_stats_all; summary_t(j).Ts_stats];
    end
    T_stats_mean = varfun(@mean, T_stats_all(:,2:end));
    T_stats_mean.Properties.VariableNames = {'N', 'min', 'max', 'mean', 'median', 'std'};
    Tn_stats_mean = varfun(@mean, Tn_stats_all(:,2:end));
    Tn_stats_mean.Properties.VariableNames = {'N', 'min', 'max', 'mean', 'median', 'std'};
    Ts_stats_mean = varfun(@mean, Ts_stats_all(:,2:end));
    Ts_stats_mean.Properties.VariableNames = {'N', 'min', 'max', 'mean', 'median', 'std'};
    sd_mean = struct();
    sd_mean.T_stats = T_stats_mean;
    sd_mean.Tn_stats = Tn_stats_mean;
    sd_mean.Ts_stats = Ts_stats_mean;
    
    % Export
        
    % mat files 
    summary_file_name = [app_config.summary_file_base_name, '.mat'];
    summary_file_path = fullfile(summary_directory_path, summary_file_name);
    save(summary_file_path, '-struct', 'sd_mean');
    
    % Export plots
    
    %%
    if Nt == 2
        fprintf('<<TFM3D postprocess>> *** Dataset has only one non-reference time point. traction kymograph will not be plotted.');
    else
        figure_title = 'traction kymograph';
        hfig = figure();

        % prepare data
        % TODO: Hardcoded time interval here. Replace with dataset driven value when dataset time values are fixed.
        dt = 240; % seconds
        time = (0:Nt-2)*dt; % ignore the first time point, which is the reference state itself.
        dx = 4; % um
        ts = summary_t(1).ts;
        x = ts.vertices(:,1);
        xbin_min = floor(min(x)/dx)*dx;
        xbin_max = ceil(max(x)/dx)*dx;
        edges = xbin_min:dx:xbin_max;
        centers = edges(1:end-1) + dx/2;
        [N, ~, bins] = histcounts(x, edges);

        T_binned = zeros(Nt-1,numel(unique(bins)));
        Tn_binned = zeros(Nt-1,numel(unique(bins)));
        Ts_binned = zeros(Nt-1,numel(unique(bins)));
        for j=1:Nt-1
            T_binned(j, :) = accumarray(bins(:), summary_t(j+1).Tmag, [], @mean)';
            Tn_binned(j, :) = accumarray(bins(:), summary_t(j+1).Tnmag, [], @mean)';
            Ts_binned(j, :) = accumarray(bins(:), summary_t(j+1).Tsmag, [], @mean)';
        end
        

        % plot
        subplot(1,3,1);
        [X, T] = meshgrid(centers, time);
        [~, h] = contourf(X,T/60, T_binned/1000, 256);
        set(h, 'EdgeColor', 'None');
        Cmax = ceil(max(T_binned(:))/1000/1)*1;
        CLim = [0 Cmax];
        set(gca, 'Clim', CLim);
        axis square;
        title('Traction (kPa)');
        xlabel('distance along channel axis (\mum)');
        ylabel('time (min)');

        subplot(1,3,2);
        [X, T] = meshgrid(centers, time);
        [~, h] = contourf(X,T/60, Tn_binned/1000, 256);
        set(h, 'EdgeColor', 'None');
        set(gca, 'Clim', CLim);
        axis square;
        title('Normal Traction (kPa)');
        xlabel('distance along channel axis (\mum)');
        ylabel('time (min)');

        subplot(1,3,3);
        [X, T] = meshgrid(centers, time);
        [~, h] = contourf(X,T/60, Ts_binned/1000, 256);
        set(h, 'EdgeColor', 'None');
        set(gca, 'Clim', CLim);
        axis square;
        position = get(gca, 'Position');
        colorbar;
        set(gca, 'Position', position);
        title('Shear Traction (kPa)');
        xlabel('distance along channel axis (\mum)');
        ylabel('time (min)');

        % Configure plot:
        plot_settings = generate_plot_settings('figure_size', [9,3]);
        hfig = configure_plot(hfig, plot_settings);

        % Export:
        figure_name = [figure_title '.png'];
        figure_path = fullfile(summary_directory_path, figure_name);
        print(figure_path, '-dpng', '-r300');
        %close(hfig);
    end
    
end

fprintf('\n<<TFM3D postprocess>> %s: Execution completed. Elapsed time is %.2f minutes.\n\n',datestr(now, date_str), toc(batch_process_timer)/60);

% end of script

