function [U, EIJ, EV, SIJ, TI, TnI, TsI] = calculateDeformationField(X, Y, Z, u, internalBoundary, Es, nus, materialModel)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Identity tensor:
I = eye(3);
% Shear modulus
calc_mu = @(E,nu)(0.5*E./(1+nu));
% 1. Lames Constant
calc_lambda = @(E,nu)(E.*nu./(1+nu)./(1-2.*nu));
% Bulk modulus
calc_Kb = @(E,nu)(E./3./(1-2.*nu));

mus = calc_mu(Es,nus);
lambdas = calc_lambda(Es,nus);
Kb = calc_Kb(Es,nus);

% Internal boundary properties
% internalBoundary = unifyMeshNormals(internalBoundary, 'alignTo', 'out'); % Make sure surface normals point outward
n  = calculateSurfaceNormals(internalBoundary); % vertex normals
nb = -n; % Forces are applied **by the cell** on the **inner surface** of the boundary

% Calculate displacement gradient at domain interior (i) and boundary (b):
duij_i = cell(3,3);
duij_b = cell(3,3);
% Also evaluate the displacement itself:
u_i = cell(3,1);
u_b = cell(3,1);
for i = 1:3
    [dui_i, dui_b, ui_i, ui_b] = calculateGradient(u{i}, X, Y, Z, internalBoundary);
    u_i{i} = ui_i;
    u_b{i} = ui_b;
    for j = 1:3
        duij_i{i,j} = dui_i{j};
        duij_b{i,j} = dui_b{j};
    end
end

% Nest interior and boundary calculations in the same data structure:
DUIJ = cell(2,1); DUIJ{1} = duij_i; DUIJ{2} = duij_b;
U    = cell(2,1); U{1} = u_i; U{2} = u_b;
EIJ  = cell(2,1);
EV   = cell(2,1);
SIJ  = cell(2,1);

% Traction stresses on the inner surface of the boundary:
TI  = cell(3,1);
TnI = cell(3,1);
TsI = cell(3,1);

for s = 1:2
    duij = DUIJ{s};
    N = numel(duij{1,1});
    dim = size(duij{1,1});

    Eij = cell(3);
    Sij = cell(3);
    for i = 1:numel(Eij)
        Eij{i} = zeros(dim);
        Sij{i} = zeros(dim);
    end
    Ev = zeros(dim);
    
    if s==2 % If boundary calculation, also calculate the traction stresses
        for i = 1:numel(TnI)
            TnI{i} = zeros(dim);
            TsI{i} = zeros(dim);
        end
    end

    for  i = 1:N

        % Deformation gradient:
        dU = [duij{1,1}(i) duij{1,2}(i) duij{1,3}(i);    ...
              duij{2,1}(i) duij{2,2}(i) duij{2,3}(i);    ...
              duij{3,1}(i) duij{3,2}(i) duij{3,3}(i);
              ];
        
        F = dU + I;

        % Right Cauchy-Green deformation tensor
        C = F' * F;  % Used for strain analysis
        % Left Cauchy-Green deformation tensor
        B = F  * F'; % Used for stress estimation

        % Lagrange strain
        E = 0.5 * (C - I);

        % Large Strain Volumetric Deformation
        % EE>0 expansion, EE<0 compaction
        J = max(1e-6, det(F)); % Guard for errors in numerical differentiation % TODO get back to here to check the number.
        Ev(i) = J - 1;

        % Stress Measures:
        switch materialModel
            case 'Neohookean Hyperelastic'
                % Cauchy stress based on Neohookean hyperelastic material model
                mu1 = mus;
                K1  = Kb;
                S  = mu1 ./ J.^(5/3) .* (B - 1/3 * trace(B) * I) + K1 * (J - 1) * I;
            case 'Linear Elastic'
                % Cauchy stress based on linear elastic material model
                % Use infinitesimal strain instead of lagrange strain
                E = 0.5 * (dU + dU');
                S = lambdas .* trace(E) .* I + 2 .* mus .* E;
            otherwise
                error('Undefined material model: %s. Currently only `neohookean` and `linearElastic` are supported.', materialModel)
        end
        
        
        for j = 1:numel(Eij)
            Eij{j}(i) = E(j);
            Sij{j}(i) = S(j);
        end
        
        if s==2 % If boundary calculation, also calculate the traction stresses
            nbi = nb(i,:); nbi = nbi(:);
            Tbi = S'*nbi;
            Tbni = (Tbi.*nbi).*nbi;
            Tbsi = Tbi - Tbni;
                
            for j = 1:numel(TnI)
                TI{j}(i)  = Tbi(j);
                TnI{j}(i) = Tbni(j);
                TsI{j}(i) = Tbsi(j);
            end
        
        end

    end
    
    EIJ{s} = Eij;
    EV{s}  = Ev;
    SIJ{s} = Sij;
    
end

end

