function export_displacement_field_to_vtk(user_input, coord, app_config)

range = user_input.process_range;
base_directory = user_input.base_directory_path;

displacement_directory_path = app_config.get_displacement_directory(base_directory);
for t = range
    
    displacement_file_path = fullfile(displacement_directory_path, app_config.generate_numbered_file_name(app_config.displacement_file_base_name, t, 'mat'));
    vtk_output_file_path   = fullfile(displacement_directory_path, app_config.generate_numbered_file_name(app_config.displacement_file_base_name, t, 'vtk'));
    
    DF = load(displacement_file_path);
    dvc_field = DF.dvc_field;
    
    mesh2ndgrid = @(x)(permute(x, [2,1,3])); 
    grid_info = struct();
    grid_info.type = 'structured_grid';
    grid_info.label = 'dvc field';
    Xvtk = mesh2ndgrid(dvc_field.X);
    Yvtk = mesh2ndgrid(dvc_field.Y);
    Zvtk = mesh2ndgrid(dvc_field.Z);
    grid_info.dimensions = size(Xvtk);
    grid_info.points = [Xvtk(:) Yvtk(:) Zvtk(:)];

    datasets = {};

    data = struct();
    data.type = 'scalar';
    data.label = 'u_mag';
    data.values = reshape(mesh2ndgrid(dvc_field.u_mag), [], 1);
    datasets{end+1} = data;

    data = struct();
    data.type = 'vector';
    data.label = 'u_vec';
    data.values = [reshape(mesh2ndgrid(dvc_field.u{1}), [], 1), ...
                   reshape(mesh2ndgrid(dvc_field.u{2}), [], 1), ...
                   reshape(mesh2ndgrid(dvc_field.u{3}), [], 1)];
    datasets{end+1} = data;

    write_vtk(vtk_output_file_path, grid_info, datasets);
    
    
    vtk_output_file_path   = fullfile(displacement_directory_path, app_config.generate_numbered_file_name([app_config.displacement_file_base_name, '_ts'], t, 'vtk'));
    
    deformed_traction_surface = deform_traction_surface(coord.traction_surface, dvc_field.u, {dvc_field.X, dvc_field.Y, dvc_field.Z});
    
    mesh2ndgrid = @(x)(permute(x, [2,1,3])); 
    grid_info = struct();
    grid_info.type = 'unstructured_grid';
    grid_info.label = 'traction surface';
    grid_info.points = deformed_traction_surface.vertices;
    grid_info.cells = deformed_traction_surface.faces;
    
    Np = size(grid_info.points, 1);
    datasets = {};
    data.type = 'scalar';
    data.label = 'dummy';
    data.values = ones(Np, 1);
    datasets{end+1} = data;
    
    write_vtk(vtk_output_file_path, grid_info, datasets);
   
end


    
end