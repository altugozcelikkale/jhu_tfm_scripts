clear;
clc;
close all;

x = linspace(-2,2,51);
y = linspace(-2,2,51);
z = linspace(-2,2,51);

[X, Y, Z] = meshgrid(x,y,z);

%% Construct boundary
boundaryEntity = 'cylinder';
switch boundaryEntity
    case 'sphere'
        radius = 1;
        Nb = 50;
        [xb,yb,zb] = sphere(Nb);
        xb = xb * radius;
        yb = yb * radius;
        zb = zb * radius;
        %Scale xb:
        xb = 6*xb;
        % Rotate around y-axis
    %     theta = pi/2;
    %     R = [cos(theta), 0 -sin(theta); 0, 1, 0; sin(theta), 0, cos(theta)];
    %     coord2 = [xb(:), yb(:), zb(:)]*R';
    %     x2 = reshape(coord2(:,1), sb);
    %     y2 = reshape(coord2(:,2), sb);
    %     z2 = reshape(coord2(:,3), sb);
        internalBoundary = surf2patch(xb,yb,zb, 'triangles');
    case 'cylinder'
        t = linspace(-2*pi, 2*pi, 100);
        boundaryProfile = 0.5+0.2*(rand(10,1)-0.5); %0.5+0.2*sin(t); %repmat(1.5,50,1);
        Nb = 5;
        [xb,yb,zb] = cylinder(boundaryProfile, Nb);
        sb = size(xb);
        %Scale zb:
        zb = 4*(zb-0.5);
        % Rotate around y-axis
        theta = pi/2;
        R = [cos(theta), 0 -sin(theta); 0, 1, 0; sin(theta), 0, cos(theta)];
        coord2 = [xb(:), yb(:), zb(:)]*R';
        x2 = reshape(coord2(:,1), sb);
        y2 = reshape(coord2(:,2), sb);
        z2 = reshape(coord2(:,3), sb);
        internalBoundary = surf2patch(x2,y2,z2, 'triangles');
    case 'box'
        outDomain = abs(X) < 2 & abs(Y) < 1.0 & abs(Z) < 1.0;
        internalBoundary = isosurface(X, Y, Z, outDomain, 0.99);
        internalBoundary.faces = fliplr(internalBoundary.faces); % ensure outward normals.
    otherwise
        error('Boundary entity: %s is not recognized', boundaryEntity);
end

internalBoundary = display_surface_mesh(internalBoundary, true);

xb = internalBoundary.vertices(:,1);
yb = internalBoundary.vertices(:,2);
zb = internalBoundary.vertices(:,3);


%%
coord = [X(:) Y(:) Z(:)];
outDomain = inpolyhedron(internalBoundary, coord);
outDomain = reshape(outDomain, size(X));
% A gaussian displacement function
A = 1;
x0 = 0; y0 = 1; z0 = 0;
s = 1;
u1 = A .* exp (-((X - x0).^2 +(Y - y0).^2+(Z - z0).^2)./ 2. /s.^2);

dudx = - u1 .* (X - x0)./s^2;
dudy = - u1 .* (Y - y0)./s^2;
dudz = - u1 .* (Z - z0)./s^2;

x0 = 0; y0 = -1; z0 = 0;
s = 1;
u2 = A .* exp (-((X - x0).^2 +(Y - y0).^2+(Z - z0).^2)./ 2. /s.^2);

u = u1 + u2;

dudx = dudx - u2 .* (X - x0)./s^2;
dudy = dudy - u2 .* (Y - y0)./s^2;
dudz = dudz - u2 .* (Z - z0)./s^2;

du{1} = dudx;
du{2} = dudy;
du{3} = dudz;


distanceFactor = 0.75; %0.1:0.1:1;
settings.isAbsoluteFactor = false;
for s = 1:numel(distanceFactor)
    fprintf('Processing %d/%d\n',  s, numel( distanceFactor));
    settings.distanceFactor = distanceFactor(s);
    % Calculate gradient
    [duic, dubc, uic, ubc] = calculateGradient(u, X, Y, Z, internalBoundary, settings); % c for 'calculated'


    % Evaluate boundary values
    ube = interp3(X,Y,Z,u,xb,yb,zb,'linear',nan);
    dube = {};
    for i=1:3
        dube{i} = interp3(X,Y,Z,du{i},xb,yb,zb,'linear',nan);
    end

    % Compare Gradient Magnitudes
    dubme = 0;
    dubmc = 0; 
    for i=1:3
        dubme = dubme + dube{i}.^2;
        dubmc = dubmc + dubc{i}.^2;
    end
    dubme = sqrt(dubme);
    dubmc = sqrt(dubmc);

    calculateRelativeError = @(xe,xc)(norm(xc(:)-xe(:))./norm(xe(:)));
    fe = sqrt(du{1}.^2 + du{2}.^2 + du{3}.^2); fe = fe(~outDomain);
    fc = sqrt(duic{1}.^2 + duic{2}.^2 + duic{3}.^2); fc = fc(~outDomain);
    dum_rel_error(s) = calculateRelativeError(fe, fc);
    dubm_rel_error(s) = calculateRelativeError(dubme, dubmc);
end

%% Diagnostic Figures
outputDirectory = fullfile('Case Studies\Scrap\test_calculateGradient');
hfig  = figure();
figureTitle = sprintf('%s - %s', 'Relative Error in Displacement Gradient', boundaryEntity);

hplot(1) = plot(distanceFactor, dum_rel_error);
hold on;
hplot(2) = plot(distanceFactor, dubm_rel_error);
hold off;

set(hplot(1), ...
    'LineStyle',      '--', ...
    'Color',      'r', ...
    'LineWidth',      1, ...
    'Marker',         'o' ...
    );

set(hplot(2), ...
    'LineStyle',      '--', ...
    'Color',      'b', ...
    'LineWidth',      1, ...
    'Marker',         's' ...
    );

legend(hplot, {'Domain Interior', 'Boundary'});


%%
figure();
h = slice(X, Y, Z, u ,0, 0, 0);
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','k',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('u_{exact}');

figure();
h = slice(X, Y, Z, uic ,0, 0, 0);
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','k',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('u_{calculated}');

figure();
h = slice(X, Y, Z, du{1} ,0, 0, 0);
colorbar;
Cmin = -1;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('du1_{exact}');

figure();
h = slice(X, Y, Z, duic{1} ,0, 0, 0);
colorbar;
Cmin = -1;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('du1_{calculated}');


figure();
h = slice(X, Y, Z, duic{1}-du{1} ,0, 0, 0);
colorbar;
Cmin = -1;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('du1_{error}');

figure();
h = patch(internalBoundary, 'FaceVertexCData',ube);
colorbar
colorbar;
Cmin = -1;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('u_{boundary, exact}');

figure();
h = patch(internalBoundary, 'FaceVertexCData',ubc);
colorbar
colorbar;
Cmin = -1;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('u_{boundary, calculated}');


figure();
h = patch(internalBoundary, 'FaceVertexCData',dube{2});
colorbar
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('du2_{boundary, exact}');


figure();
h = patch(internalBoundary, 'FaceVertexCData',dubc{2});
colorbar
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','none',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(0,90);
title('du2_{boundary, calculated}');
%%
figure();
h = patch(internalBoundary, 'FaceVertexCData',dubme);
colorbar
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','k',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(-45,25);
title('dum_{boundary, exact}');


figure();
h = patch(internalBoundary, 'FaceVertexCData',dubmc);
colorbar
colorbar;
Cmin = 0;
Cmax =1;
set(h,'FaceColor','interp',...
    'EdgeColor','k',...
    'DiffuseStrength',.8);

set(gca, ...
        'CLim'         , [Cmin Cmax] ...
        )
daspect([1 1 1]);
view(-45,25);
title('dum_{boundary, calculated}');

