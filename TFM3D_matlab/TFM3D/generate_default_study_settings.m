function settings = generate_default_study_settings()
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
settings = struct();

% Prepare dataset
settings.process_range_str = [];

% DVC (Displacement Field) 
% getDefaultDVCSettings returns the default set of parameters suitable to
% run the FIDVC code of Franck Lab
% PRESET CONSTANTS
% maxIterations = 20; % maximum number of iterations
% dm = 8; % desired output mesh spacing
% convergenceCrit = [0.25, 0.5, 0.0625]; % convergence criteria
% % Modified: Altug. Prevent window refinement in cummulative calculations:
% %convergenceCrit = [1e-16, 0.5, 0.0625]; % convergence criteria
% ccThreshold = 1e-4; % bad cross-correlation threshold
settings.dvc.interrogationWindowSize = [64 64 64];
settings.dvc.minInterrogationWindowSize = 16;
settings.dvc.outputMeshSpacing = settings.dvc.minInterrogationWindowSize ./ 4;
settings.dvc.maxIterations = 20;
settings.dvc.minInterrogationWindowSize = 32;
settings.dvc.outputMeshSpacing = 8;
settings.dvc.refinementErrorThreshold = 0.25;
settings.dvc.globalErrorThreshold = 0.0625;
settings.dvc.ccThreshold = 1e-4;
settings.dvc.incORcum = 'cum'; % cumulative vs increment matching. See funIDVC() for details.

% Postprocessing of displacement by median filtering for edge preserving
% smoothing.
settings.perform_displacement_filtering = true;
settings.displacement_filter_method = 'smoothn'; % Currently only 


% TFM (Force Field) 
settings.tfm.E = [];
settings.tfm.nu = 0.48;
settings.tfm.material_model = 'Linear Elastic';



end

