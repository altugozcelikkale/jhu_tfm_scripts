function plot_settings = generate_plot_settings(varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%% Parse input arguments
p = inputParser;
p.FunctionName = mfilename();
p.addParameter('figure_size', [6 6], @(x)(isnumeric(x) & all(size(x) == [1 2])));
p.addParameter('resolution', 300, @(x)(isscalar(x) & isPositiveIntegerValuedNumeric(x)));
p.parse(varargin{:})


%% Default plot configuration:
plot_settings = struct();
plot_settings.figure_units  = 'Inches';
plot_settings.figure_size   = p.Results.figure_size;
plot_settings.figure_position = [3 3 plot_settings.figure_size];
plot_settings.paper_units  = 'Inches';
% On paper
plot_settings.paper_size    = plot_settings.figure_size;
plot_settings.axes_size     = [0.9 0.9];
plot_settings.axes_position = [0.05 0.05 plot_settings.axes_size];
plot_settings.resolution    = p.Results.resolution; % DPI


end

