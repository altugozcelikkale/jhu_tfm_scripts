
library_directory = fileparts(mfilename('fullpath'));
cd(library_directory);

fprintf(repmat('=',1,90));
fprintf('\n');
fprintf('[\bWelcome!]\b\n')
fprintf('==> This script will setup Matlab path to exclusively include the current folder and its subfolders...\n');
fprintf('==> Library folder is: %s...\n', library_directory);

% Save current matlab path into file: 
% if ~exist('previous_matlab_path', 'file')
%     savepath('previous_matlab_path.m');
%     fprintf('==> Execute `restore_previous_matlab_path.m` to revert back to your earlier Matlab path. (This step is automatic when matlab exits while in current directory)\n');
% else
%     error('`previous_matlab_path.m` already exists in the current directory. Either rename or remove the file and run `activate_environment` again');
% end
%% Add current folder and its subfolders to MATLAB search path:
restoredefaultpath; % This is where we lose the previous matlab path. Necessary, but use with caution.
refresh_search_path(library_directory, {'__exclude__', '.git'});

%% Start with clean workspace ...
clear;

%% ... except for some 'global' constants we may want to define:
% TODO: Include here instance (singleton?)
% of a class composed of all static constant properties.
fprintf('\n[\bEnvironment activated. You may now use TFM3D.]\b\n');
fprintf(repmat('=',1,90));
fprintf('\n\n');