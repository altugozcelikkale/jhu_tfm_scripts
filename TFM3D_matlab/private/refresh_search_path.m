function refresh_search_path(varargin)
% REFRESHSEARCHPATH Add scripts in the specified directory to MATLAB path
%    INPUT:
%      OPTIONAL: A string representing the folder that contains scripts and
%      subprojects to be included or refreshed in MATLAB path. Defaults to 
%      the folder containing the startup script.
%      OPTIONAL: A cell array of strings that specify an ignore list. The
%      paths generated by recursive search of the library base folder are
%      filtered if they contain any one of the specified strings. This
%      is useful to prevent certain files or folders from being added to
%      path inadvertently by filtering based on their path. Defaults to
%      empty cell array i.e no filtering. Requires library
%      base folder to be specified.
%    OUTPUT:
%      none.
%
library_folder = fileparts(which('startup'));
ignore_list = {};
if length(varargin) == 1
    library_folder = fullfile(varargin{1});
elseif length(varargin) == 2
    library_folder = fullfile(varargin{1});
    ignore_list = varargin{2};
end
paths = strsplit(genpath(library_folder), ';');  % include subfolders too.
% Filter paths containing a special string e.g. '__excluded__'
for i = 1:length(ignore_list)
    ignore_str = ignore_list{i};
    not_ignored = cellfun(@isempty,strfind(paths, ignore_str));
    paths = paths(not_ignored);
end
addpath(strjoin(paths, ';'));

% Special measures to priortize the main folders for certain packages. E.g. exportfig is used in multiple thirdparty packages, we would like to use the original one in 'export_fig' folder 

end