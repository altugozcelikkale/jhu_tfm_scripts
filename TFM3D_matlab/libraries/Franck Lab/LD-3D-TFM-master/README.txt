﻿Date:       May 5th, 2014

Description:  This package contains the files for the 3-D Large Deformed Traction Force Microscopy method as published in

Toyjanova J., Bar-Kochba E., López-Fagundo C., Reichner J., 
Hoffman-Kim D, Franck, C. (2014) High Resolution, Large Deformation 3D
Traction Force Microscopy. PLoS ONE 9(4): e90976. 
doi:10.1371/journal.pone.0090976

The main example file to see how the package runs is exampleRunFile.m.

Files: 

Main files:
calculateNormals.m
calculateSurfaceUi.m
findSurface.m
fun3DTFM.m
removeOutliers.m


Example run files:
exampleRunFile.m
vol00.mat
vol01.mat 

File from the MATLAB file exchange:
gridfit.m
inpaint_nans.m
inpaint_nans3.m


History:    1.00 - Release

Bugs:       None discovered yet.