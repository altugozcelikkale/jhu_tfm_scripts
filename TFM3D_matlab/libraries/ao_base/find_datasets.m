function dataset_base_dirs = find_datasets(base_dir, file_name_pattern)
    initial_dir = pwd();
    cd(base_dir);
    search_pattern = ['**/', file_name_pattern];
    path_list = dir(search_pattern);
    dataset_base_dirs = {path_list(:).folder};
    cd(initial_dir);
end

