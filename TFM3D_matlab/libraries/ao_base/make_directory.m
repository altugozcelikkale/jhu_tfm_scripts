function make_directory(path, varargin)
    
    [~, file, ext] = fileparts(path);
    if ~(isempty(file) && isempty(ext))
        path(end+1) = filesep;
        %error('Not a valid folder path: %s. Did you forget to include the file separator at the end of the path?', path);
    end

    if isempty(varargin)
        nonempty_option = 'do_nothing';
    else
        nonempty_option = varargin{1};
    end

    if ~exist(path, 'dir')
        mkdir(path)
    elseif length(dir(path)) ~= 2 % non-empty folder
        switch nonempty_option
            case 'overwrite'
                rmdir(path, 's')
                mkdir(path)
            case 'backup'
                folder_path = fileparts(path); % Sterilize the path string from trailing file separators
                [parent_folder_path, folder_name, ~] = fileparts(folder_path);
                backup_path = fullfile(parent_folder_path, sprintf('%s_bak_%s', folder_name, datetime('now','TimeZone','local','Format','ymd_HHmmss')));
                copyfile(path, backup_path);
                rmdir(path, 's')
                mkdir(path)
            case 'do_nothing'
                % do nothing
            otherwise
                error('Unidentified nonempty_option while executing make_directory(): %s', nonempty_option);
        end
    end
                
end % function
