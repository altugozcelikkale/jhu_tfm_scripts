function stats = calculate_descriptive_statistics(response, varargin)
% CALCULATE_DESCRIPTIVE_STATISTICS calculate various descriptive statistics
% of response optionally grouped by categories.
%   stats = CALCULATE_DESCRIPTIVE_STATISTICS(response) returns a table
%   `stats` that summarizes the size, mean, median and standard deviation
%   of the response vector.
%
%   See also .

%% Default values and validators
response = response(:);
calc_multi_stats = @(x)({numel(x), min(x), max(x), mean(x), median(x), std(x, 0)});
calc_multi_stats_column_headings = {'N', 'min', 'max', 'mean', 'median', 'std'};
stats_fun_default = {calc_multi_stats, calc_multi_stats_column_headings};
stats_fun_validator = @(x)(isa(x{1}, 'function_handle') & iscellstr(x{2}));

%% Parse input arguments
p = inputParser;
p.FunctionName = 'calculate_descriptive_statistics';
p.addRequired('response', @(x)(isnumeric(x)));
p.addParameter('categories', categorical(zeros(size(response))), @(x)(iscategorical(x)));
p.addParameter('stats_fun', stats_fun_default, stats_fun_validator)

p.parse(response, varargin{:})

categories = p.Results.categories;
stats_function = p.Results.stats_fun{1};
stats_columns = p.Results.stats_fun{2};
Nstats = numel(stats_columns);

% Check for input argument consistency:
dummy_vec = 1:3;
dummy_stats = stats_function(dummy_vec);
assert(numel(dummy_stats) == Nstats, 'The size of the array returned by stats function must match the number of column names (%d) provided', Nstats);

%% Calculate descriptive statistics
[groups, group_ids] = findgroups(categories);
stats = splitapply(stats_function, response, groups);
stats = cell2table(stats, 'VariableNames', stats_columns);
stats.treatment_group = group_ids;
% Reorder to bring the last column to front:
stats = stats(:, [end 1:end-1]);

end

