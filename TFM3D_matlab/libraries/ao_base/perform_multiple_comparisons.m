function varargout = perform_multiple_comparisons(response, categories, varargin)
% PERFORM_MULTIPLE_COMPARISONS performs 
%   stats = CALCULATE_DESCRIPTIVE_STATISTICS(response) returns a table
%   `stats` that summarizes the size, mean, median and standard deviation
%   of the response vector.
%
%   See also .

%% Default values and validators
response = response(:);
categories = categories(:);

%% Parse input arguments
p = inputParser;
p.FunctionName = 'perform_multiple_comparisons';
p.addRequired('response', @isnumeric);
p.addRequired('categories', @iscategorical);
p.addParameter('type', 'ranksum', @ischar);

p.parse(response, categories, varargin{:})
response = p.Results.response;
categories = p.Results.categories;
type = p.Results.type;

% Check for input argument consistency:
% assert ...


%% Test for statistical significance
[~, group_ids] = findgroups(categories);


group_combinations = combnk(cellstr(group_ids),2);
groups1  = categorical(group_combinations(:,1));
groups2  = categorical(group_combinations(:,2));
Nc = numel(groups1);

p_value = zeros(size(groups1));
p_value = p_value(:);
comparison_string = cell(size(groups1));
comparison_string = comparison_string(:);
for i = 1:Nc
    sample1  = response(categories == groups1(i));
    sample2  = response(categories == groups2(i));
    if strcmp(type, 'ranksum')
        p_value(i) = ranksum(sample1, sample2);
    elseif strcmp(type, 'ttest')
        [~, p_value(i)] = ttest2(sample1, sample2);
    end
    comparison_string{i} = sprintf('%s VS %s', groups1(i), groups2(i));
end
varargout{1} = table(comparison_string, p_value, 'VariableNames', {'multiple_comparisons', 'p_value'});
if length(varargout) == 2 
    % TODO: Add ANOVA as an optional output.
    % ANOVA based on Kruskal-Wallis median test:
    %[p,tbl,stats] = kruskalwallis(filtered_summary_table{:, variable}, activation_categories);
    %disp(p);
end

end

