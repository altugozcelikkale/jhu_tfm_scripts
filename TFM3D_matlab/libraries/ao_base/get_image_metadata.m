function varargout = get_image_metadata( file_path )
    %GET_IMAGE_METADATA extracts metadata from an image file.
    % Option 1. Read image without loading all in the memory
    reader = bfGetReader(file_path);
    omeMeta = reader.getMetadataStore();

    metadata = struct();
    % Voxel Dimensions
    metadata.Nx = double(omeMeta.getPixelsSizeX(0).getValue());
    metadata.Ny = double(omeMeta.getPixelsSizeY(0).getValue());
    metadata.Nz = double(omeMeta.getPixelsSizeZ(0).getValue());
    metadata.Nc = double(omeMeta.getPixelsSizeC(0).getValue());
    metadata.Nt = double(omeMeta.getPixelsSizeT(0).getValue());

    % Physical Calibration
    if isempty(omeMeta.getPixelsPhysicalSizeX(0))
        metadata.dx = 1;
    else
        metadata.dx = double(omeMeta.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROMETER));
    end
   
    if isempty(omeMeta.getPixelsPhysicalSizeY(0))
        metadata.dy = 1;
    else
        metadata.dy = double(omeMeta.getPixelsPhysicalSizeY(0).value(ome.units.UNITS.MICROMETER));
    end
    
    if isempty(omeMeta.getPixelsPhysicalSizeZ(0))
        metadata.dz = 1;
    else
        metadata.dz = double(omeMeta.getPixelsPhysicalSizeZ(0).value(ome.units.UNITS.MICROMETER));
    end
    
    if isempty(omeMeta.getPixelsTimeIncrement(0))
        metadata.dt = 1;
    else
        metadata.dt = double(omeMeta.getPixelsTimeIncrement(0).value(ome.units.UNITS.SECOND));
    end
    
    varargout{1} = metadata;
    varargout{2} = omeMeta; % return the complete ome.tiff metadata if requested.

end

