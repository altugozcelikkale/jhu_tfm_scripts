function I = read_multidimensional_image(image_path, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
md = get_image_metadata(image_path);
series_index = 0;
ic = 1:md.Nc;
iz = 1:md.Nz;
it = 1:md.Nt;
if length(varargin) >= 1
    if length(varargin) == 1
        series_index = varargin{1};
    elseif length(varargin) == 4
        ic = varargin{2};
        iz = varargin{3};
        it = varargin{4};
    else
        error('Either 1 (series_index) or 4 (series_index, ic, iz, it) optional arguments are expected')
    end
end

reader = bfGetReader(image_path);
reader.setSeries(series_index);


Nc = length(ic);
Nz = length(iz);
Nt = length(it);
% Sneek peek into the image array to determine the data type:
single_plane = bfGetPlane(reader, 1);
I = zeros(md.Ny, md.Nx, Nc, Nz, Nt, 'like', single_plane);

for i = 1:Nc
    ii = ic(i);
    for j = 1:Nz
        jj = iz(j);
        for k = 1:Nt
            kk = it(k);
            try
                currentPlane = reader.getIndex(jj-1, ii-1, kk-1) + 1;
            catch exception
                fprintf(2, '\nError: Unable to get image plane with(ic, iz, it) = (%d, %d, %d). Are any of the indices out of bound? See below for details.\n\n', ii, jj, kk);
                rethrow(exception);
            end
            I(:,:,i,j,k) = bfGetPlane(reader, currentPlane);
        end
    end
end
reader.close()

end

