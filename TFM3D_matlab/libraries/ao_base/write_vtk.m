function write_vtk(path, grid_info, data, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% CALCULATE_DESCRIPTIVE_STATISTICS calculate various descriptive statistics
% of response optionally grouped by categories.
%   stats = CALCULATE_DESCRIPTIVE_STATISTICS(response) returns a table
%   `stats` that summarizes the size, mean, median and standard deviation
%   of the response vector.
%
%   See also .

% Default values and validators
% grid_types = {'structured_grid', 'unstructured_grid'};

%% Parse input arguments
p = inputParser;
p.FunctionName = 'write_vtk';
p.addRequired('path', @(x)(ischar(x)));
p.addRequired('grid_info', @(x)(isstruct(x)));
p.addRequired('data', @(x)(iscell(x) || isstruct(x)));
p.addParameter('binary', false, @islogical);
p.parse(path, grid_info, data, varargin{:})

path = p.Results.path;
grid_info = p.Results.grid_info;
data = p.Results.data;
is_binary = p.Results.binary;

% Check for input argument consistency:
if strcmp(grid_info.type, 'structured_grid')
    required_fields = {'points'};
    fields_match = cellfun(@(x)(any(strcmp(x, fieldnames(grid_info)))), required_fields);
    if ~all(fields_match)
        fprintf('Looking for fields:\n');
        fprintf('%s\n', required_fields{:});
        error('Required field(s) missing in grid info');
    end
elseif strcmp(grid_info.type, 'unstructured_grid')
    required_fields = {'points', 'cells'};
    fields_match = cellfun(@(x)(any(strcmp(x, fieldnames(grid_info)))), required_fields);
    if ~all(fields_match)
        fprintf('Looking for fields:\n');
        fprintf('%s\n', required_fields{:});
        error('Required field(s) missing in grid info');
    end
    
else
    error('\nUnrecognized grid type: %s\n', grid_info.type);
end
if ~isfield(grid_info, 'label')
    grid_info.label = '';
end


% TODO: input check for data as well. However above is a bit clumsy isn't
% it...
if isstruct(data)
    data = {data};
end

%% Generate vtk file:
fid = fopen(path, 'w');
% write header
fprintf(fid, '# vtk DataFile Version 2.0\n');
fprintf(fid, '%s\n', grid_info.label);
if is_binary
    fprintf(fid, 'BINARY\n');
else
    fprintf(fid, 'ASCII\n');
end
% write the grid information
if strcmp(grid_info.type, 'structured_grid')
    fprintf(fid, 'DATASET STRUCTURED_GRID\n');
    dim = grid_info.dimensions;
    fprintf(fid, 'DIMENSIONS %d %d %d\n', dim(1), dim(2), dim(3));
    Np = prod(dim);
    fprintf(fid, 'POINTS %d float\n', Np);
    data_to_write = grid_info.points;
    dump_data_to_file(fid, data_to_write, is_binary);
    
elseif strcmp(grid_info.type, 'unstructured_grid')
    fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
    Np = size(grid_info.points, 1);
    fprintf(fid, 'POINTS %d float\n', Np);
    data_to_write = grid_info.points;
    dump_data_to_file(fid, data_to_write, is_binary);
    Ncell = size(grid_info.cells, 1);
    fprintf(fid, 'CELLS %d %d\n', Ncell, Ncell*4); % Currently only triangular meshes are supported. TODO: Put a warning here.
    data_to_write = int32([repmat(3, Ncell, 1) grid_info.cells-1]);
    dump_data_to_file(fid, data_to_write, is_binary);
    fprintf(fid, 'CELL_TYPES %d\n', Ncell);
    data_to_write = int32(repmat(5, Ncell, 1));
    dump_data_to_file(fid, data_to_write, is_binary);
end

% Write point data
% TODO: Only point data, and not cell data, are
% supported at the moment.
fprintf(fid, 'POINT_DATA %d\n', Np);
for i = 1:length(data)
    dd = data{i};
    if strcmp(dd.type, 'scalar')
        fprintf(fid, '\nSCALARS %s float\n', dd.label);
        fprintf(fid, 'LOOKUP_TABLE default\n');
    elseif strcmp(dd.type, 'vector')
        fprintf(fid, '\nVECTORS %s float\n', dd.label);
    else
        error('\nUnrecognized data type string: %s. Must be either `scalar` or `vector`\n', dd.type);
    end
    data_to_write = dd.values;
    dump_data_to_file(fid, data_to_write, is_binary);
end

fclose(fid);
end


function dump_data_to_file(fid, data_array, is_binary)
    type_map_ascii = containers.Map();
    type_map_ascii('double') = '%f';
    type_map_ascii('integer') = '%d';
    type_map_ascii('int32') = '%d';
    
    
    type_map_binary = containers.Map();
    type_map_binary('double') = 'float';
    type_map_binary('integer') = 'int';
    type_map_binary('int32') = 'int';
    % TODO Implement more datatypes e.g. unsigned to save memory. 
    
    if is_binary
        data_array = data_array'; % The data is written in column-major ordering
        data_type = type_map_binary(class(data_array));
        fwrite(fid, data_array, data_type, 'b');
        fprintf(fid, '\n\n');
    else
        [Nr, Nc] = size(data_array);
        data_type = type_map_ascii(class(data_array));
        for i = 1:Nr
            for j = 1:Nc
                fprintf(fid, [data_type, ' '], data_array(i,j));
            end
            fprintf(fid, '\n');
        end
    end
    
end
