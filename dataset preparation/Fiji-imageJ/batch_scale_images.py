import os
from math import floor

from ij import IJ
from ij.io import DirectoryChooser
from loci.plugins import BF
from loci.plugins.in import ImporterOptions

target_scale = 0.2 # um/pixel
# scale = [1.0293, 1.0293, 1.5]

manual_file_selection = False # If true, use file_list to specify the files to be processed.
file_list = \
["cells field1.nd2",
"cells field3.nd2",
"cells field4.nd2",
"cells field15.nd2",
"cells field20.nd2",
"cells field23.nd2",
"field1.nd2",
"field3.nd2",
"field4.nd2",
"field15.nd2",
"field20.nd2",
"field23.nd2"
]





# Choose a directory with lots of tif stacks
dc = DirectoryChooser("Choose directory with stacks")
srcDir = dc.getDirectory()

if not manual_file_selection:
	dirlist = os.listdir(srcDir)
	file_list = [item for item in dirlist if os.path.isfile(os.path.join(srcDir, item))]

opt = ImporterOptions()
opt.setVirtual(True)

for item in file_list:
	path = os.path.join(srcDir, item)
	opt.setId(path)
	vimp = BF.openImagePlus(opt)[0]

	print "Processing %s"%(os.path.basename(path))
	
	cal = vimp.getCalibration()
	cal.setUnit('um')
	cal.setTimeUnit('sec')
	original_scale = [cal.pixelWidth, cal.pixelHeight, cal.pixelDepth]
	scale = [i/target_scale for i in original_scale]

	width  = floor(vimp.getWidth()*scale[0])
	height = floor(vimp.getHeight()*scale[1])
	depth  = floor(vimp.getNSlices()*scale[2])
    
	cmd_args = "x=%f y=%f z=%f width=%d height=%d depth=%d interpolation=Bicubic average create"% \
	(scale[0], scale[1], scale[2], width, height, depth)
	
	IJ.run(vimp, "Scale...", cmd_args);
	imp = IJ.getImage()
	output_path = os.path.splitext(path)[0] + '_scaled.tif'
	IJ.save(imp, output_path)
	vimp.close()
	imp.close()
	print "\tDone..."

print "Execution completed"
