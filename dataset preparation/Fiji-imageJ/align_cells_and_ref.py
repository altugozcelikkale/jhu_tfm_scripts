import os

from ij import IJ, WindowManager
from ij.io import DirectoryChooser


def run_descriptor_based_registration_2d_3d(settings={}):

	default_settings = {}
	default_settings['first_image_name'] = 'ref.tif'
	default_settings['second_image_name'] = 'cell_t1.tif'
	default_settings['brightness_of'] = '[Advanced ...]'
	default_settings['approximate_size'] = '[Advanced ...]'
	default_settings['brightness_of'] = '[Advanced ...]'
	default_settings['type_of_detections'] = '[Maxima only]'
	default_settings['subpixel_localization'] = '[3-dimensional quadratic fit]'
	default_settings['transformation_model'] = '[Rigid (3d)]'
	default_settings['images_pre-alignment'] = '[Not prealigned]'
	default_settings['number_of_neighbors'] = 3
	default_settings['redundancy'] = 1
	default_settings['significance'] = 3
	default_settings['allowed_error_for_ransac'] = 5
	default_settings['choose_registration_channel_for_image_1'] = 1
	default_settings['choose_registration_channel_for_image_2'] = 1
	default_settings['is_create_overlayed'] = 'create_overlayed'
	default_settings['is_add_point_rois'] = 'add_point_rois'
	default_settings['detection_sigma'] = 3.0
	default_settings['threshold'] = 0.05

	# Also add options for operation of this function:
	default_settings['save_log_file'] = True

	# get process directory if not specified in the settings
	if 'process_directory' not in settings.keys():
		# Choose a directory with lots of tif stacks
		dc = DirectoryChooser("Choose directory for saving output files")
		settings['process_directory'] = dc.getDirectory()

	# Fill in missing values in settings, if any, from default values
	for key, value in default_settings.iteritems():
		if key not in settings.keys():
			settings[key] = value
	
	arguments_string = "first_image={first_image_name} second_image={second_image_name} brightness_of={brightness_of} " + \
	                   "approximate_size={approximate_size} type_of_detections={type_of_detections} subpixel_localization={subpixel_localization} " + \
	                   "transformation_model={transformation_model} images_pre-alignemnt={images_pre-alignment} number_of_neighbors={number_of_neighbors} " + \
	                   "redundancy={redundancy} significance={significance} allowed_error_for_ransac={allowed_error_for_ransac} " + \
	                   "choose_registration_channel_for_image_1={choose_registration_channel_for_image_1} choose_registration_channel_for_image_2={choose_registration_channel_for_image_2} " + \
	                   "{is_create_overlayed} {is_add_point_rois} detection_sigma={detection_sigma} threshold={threshold}"
	
	arguments_string_filled = arguments_string.format(**settings)

	log_window = WindowManager.getWindow('Log')
	if log_window is not None:
		log_window.close()

	IJ.log('Running descriptor-based registration (2d/3d) with the following settings:')
	IJ.log(arguments_string_filled)
	
	IJ.run("Descriptor-based registration (2d/3d)", arguments_string_filled);

	imp_fused = IJ.getImage()

	aligned_image_base_name = os.path.splitext(settings['first_image_name'])[0]
	ref_image_base_name = os.path.splitext(settings['second_image_name'])[0]
	output_file_base_name = 'fused_{}_to_{}'.format(aligned_image_base_name, ref_image_base_name)
	
	output_file_name = output_file_base_name  + ".tif"
	output_path = os.path.join(settings['process_directory'], output_file_name)
	IJ.saveAsTiff(imp_fused, output_path)
	print(">>>> File saved: {}".format(output_file_name))

	if settings['save_log_file'] == True:
		log_contents = IJ.getLog()
		output_file_name = output_file_base_name + ".log"
		output_path = os.path.join(settings['process_directory'], output_file_name)
		with open(output_path, 'w') as fid:
			fid.write(log_contents)
		print(">>>> File saved: {}".format(output_file_name))

if __name__ in ['__main__', '__builtin__']:

	run_descriptor_based_registration_2d_3d()