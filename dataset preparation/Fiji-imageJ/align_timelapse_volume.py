import os

from ij import IJ, WindowManager
from ij.io import DirectoryChooser


def run_descriptor_based_registration_2d_3d_t(settings={}):

	default_settings = {}
	default_settings['series_of_images'] = 'cell_with_ref.tif'
	default_settings['brightness_of'] = '[Advanced ...]'
	default_settings['approximate_size'] = '[Advanced ...]'
	default_settings['brightness_of'] = '[Advanced ...]'
	default_settings['type_of_detections'] = '[Maxima only]'
	default_settings['subpixel_localization'] = '[3-dimensional quadratic fit]'
	default_settings['transformation_model'] = '[Translation (3d)]'
	default_settings['images_pre-alignment'] = 'images_are_roughly_aligned'
	default_settings['number_of_neighbors'] = 3
	default_settings['redundancy'] = 1
	default_settings['significance'] = 3
	default_settings['allowed_error_for_ransac'] = 5
	default_settings['global_optimization'] = "[All-to-all matching with range ('reasonable' global optimization)]"
	default_settings['range'] = 5
	default_settings['choose_registration_channel'] = 1
	default_settings['image'] = '[Fuse and display]'
	default_settings['detection_sigma'] = 6.0
	default_settings['threshold'] = 0.07

	# Fill in missing values in settings, if any, from default values
	for key, value in default_settings.iteritems():
		if key not in settings.keys():
			settings[key] = value

	imp = WindowManager.getImage(settings['series_of_images'])
	cal = imp.getCalibration()
	
	arguments_string = "series_of_images={series_of_images} brightness_of={brightness_of} " + \
	                   "approximate_size={approximate_size} type_of_detections={type_of_detections} subpixel_localization={subpixel_localization} " + \
	                   "transformation_model={transformation_model} {images_pre-alignment} number_of_neighbors={number_of_neighbors} " + \
	                   "redundancy={redundancy} significance={significance} allowed_error_for_ransac={allowed_error_for_ransac} global_optimization={global_optimization} range={range} " + \
	                   "choose_registration_channel={choose_registration_channel} detection_sigma={detection_sigma} threshold={threshold} image={image}"
	
	arguments_string_filled = arguments_string.format(**settings)

	log_window = WindowManager.getWindow('Log')
	if log_window is not None:
		log_window.close()

	IJ.log('Running descriptor-based series registration (2d/3d + t) with the following settings:')
	IJ.log(arguments_string_filled)
	
	IJ.run("Descriptor-based series registration (2d/3d + t)", arguments_string_filled);
	imp_fused = IJ.getImage()
	cal_fused = imp_fused.getCalibration()
	cal_fused.setUnit('um')
	cal_fused.setTimeUnit('sec')
	cal_fused.pixelWidth = cal.pixelWidth
	cal_fused.pixelHeight = cal.pixelHeight
	cal_fused.pixelDepth = cal.pixelDepth
	cal_fused.frameInterval = cal.frameInterval

if __name__ in ['__main__', '__builtin__']:

	run_descriptor_based_registration_2d_3d_t()